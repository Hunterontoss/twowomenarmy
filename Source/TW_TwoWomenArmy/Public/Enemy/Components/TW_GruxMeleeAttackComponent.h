#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_GruxMeleeAttackComponent.generated.h"

class UBoxComponent;
class ATW_EnemyAIController;
class USoundCue;
class UParticleSystem;

UENUM(BlueprintType)
enum class  EWeaponHandType : uint8
{
	EAT_Left UMETA(DisplayName = "Left"),
	EAT_Right UMETA(DisplayName = "Right"),
	EAT_MAX UMETA(DisplayName = "DefaultMax")
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_GruxMeleeAttackComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_GruxMeleeAttackComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Animation
	///

public:
	UFUNCTION(BlueprintCallable)
	void PlayAttackMontage(FName Section, float PlayRate);

	UFUNCTION(BlueprintPure)
	FName GetAttackSectionName();
	
protected:
	/** Montage containing different attacks */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UAnimMontage* AttackMontage;

private:
	/** The four attack montage section names */
	FName AttackLFast = "AttackLF";
	FName AttackRFast = "AttackRF";
	FName AttackL = "AttackL";
	FName AttackR = "AttackR";

	/////////////////////////////////////////////////////////////////////////////////////
	/// Collision
	///

public:
	UFUNCTION()
	void OnLeftWeaponOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
	void OnRightWeaponOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	UFUNCTION(BlueprintCallable)
	void ActivateWeapon(const EWeaponHandType Hand);
	UFUNCTION(BlueprintCallable)
	void DeactivateWeapon(const EWeaponHandType Hand);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Combat)
	UBoxComponent* LeftWeaponCollision;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Combat)
	UBoxComponent* RightWeaponCollision;
	/////////////////////////////////////////////////////////////////////////////////////
	/// Setup
	///
private:
	void SetUpCollision() const;
	void SetUpBinding() const;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Damage
	///

public:
	void DoDamage(AActor* Victim);
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float BaseDamage = 32.f;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Effects
	///

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	USoundCue* MeleeImpactSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UParticleSystem* BloodParticles;

protected:

	void SpawnBlood(FName SocketName) const;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	FName LeftWeaponSocket = FName("FX_Trail_L_01");

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	FName RightWeaponSocket = FName("FX_Trail_R_01");
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Limit Attack 
	///
public:
	void ResetCanAttack();

protected:
	UPROPERTY(VisibleAnywhere, Category = Combat)
	bool bCanAttack = true;

	/** Minimum wait time between attacks */
	UPROPERTY(EditAnywhere, Category = Combat)
	float AttackWaitTime = 1.f;

private:
	FTimerHandle AttackWaitTimer;
	
	/////////////////////////////////////////////////////////////////////////////////////
private:
	ATW_EnemyAIController* GetEnemyAIController() const;
	/////////////////////////////////////////////////////////////////////////////////////
};
