#include "Players/TW_BaseCharacter.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/TextRenderComponent.h"
#include "Components/WidgetComponent.h"
#include "Enemy/TW_EnemyAIController.h"
#include "Gameframework/SpringArmComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "TWComponents/CrosshairAimFactorComponent.h"
#include "GameFramework/Actor.h"
#include "TWComponents/TW_FootstepComponent.h"
#include "TWComponents/TW_HealthComponent.h"
#include "TWComponents/TW_InventoryComponent.h"
#include "TWComponents/Managers/TW_CameraManagerComponent.h"
#include "TWComponents/Managers/TW_ShooterCharacterManager.h"
#include "TWComponents/Managers/TW_WeaponManagerComponent.h"
#include "TWItems/TW_BaseAmmo.h"
#include "TWItems/TW_BaseItem.h"
#include "TWItems/TW_BaseWeapon.h"

ATW_BaseCharacter::ATW_BaseCharacter() : bAimingButtonPressed(false), BaseTurnRate{80.f}, BaseLookupRate{80.f},
	bShouldTraceForItems(false), OverlappedItemCount(0), bCrouching(false), BaseMovementSpeed(600.f),
	CrouchMovementSpeed(300.f), StandingCapsuleHalfHeight(88.f), CrouchingCapsuleHalfHeight(44.f),
	BaseGroundFriction(2.f), CrouchingGroundFriction(100.f)
{
	PrimaryActorTick.bCanEverTick = true;

	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(GetRootComponent());
	CameraBoom->TargetArmLength = 250.f;
	CameraBoom->bUsePawnControlRotation = true;
	CameraBoom->SocketOffset = FVector(0.f, 55.f, 75.f);
	
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	FollowCamera->bUsePawnControlRotation = false;

	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = true;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.f, 540.f, 0.f);
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	CrosshairAimFactorComponent = CreateDefaultSubobject<UCrosshairAimFactorComponent>("AimFactorComponent");
	WeaponManager = CreateDefaultSubobject<UTW_WeaponManagerComponent>("WeaponManager");
	TWCameraManager = CreateDefaultSubobject<UTW_CameraManagerComponent>("TWCameraManager");
	TWCharacterManager = CreateDefaultSubobject<UTW_ShooterCharacterManager>(TEXT("CharacterManager"));
	TWInventoryComp = CreateDefaultSubobject<UTW_InventoryComponent>(TEXT("InventoryComp"));

	HandSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("HandSceneComp"));

	HealthComponent = CreateDefaultSubobject<UTW_HealthComponent>(TEXT("HealthComp"));
	TextHealthComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextHealthComp"));
	TextHealthComponent->SetupAttachment(GetRootComponent());
	/////////////////////////////////////////////////////////////////////////////////////
	/// Pickup Interpolation
	///

	WeaponInterpComp = CreateDefaultSubobject<USceneComponent>(TEXT("Weapon Interpolation Component"));
	WeaponInterpComp->SetupAttachment(GetCameraComponent());

	InterpComp1 = CreateDefaultSubobject<USceneComponent>(TEXT("Interpolation Component 1"));
	InterpComp1->SetupAttachment(GetCameraComponent());

	InterpComp2 = CreateDefaultSubobject<USceneComponent>(TEXT("Interpolation Component 2"));
	InterpComp2->SetupAttachment(GetCameraComponent());

	InterpComp3 = CreateDefaultSubobject<USceneComponent>(TEXT("Interpolation Component 3"));
	InterpComp3->SetupAttachment(GetCameraComponent());

	InterpComp4 = CreateDefaultSubobject<USceneComponent>(TEXT("Interpolation Component 4"));
	InterpComp4->SetupAttachment(GetCameraComponent());

	InterpComp5 = CreateDefaultSubobject<USceneComponent>(TEXT("Interpolation Component 5"));
	InterpComp5->SetupAttachment(GetCameraComponent());

	InterpComp6 = CreateDefaultSubobject<USceneComponent>(TEXT("Interpolation Component 6"));
	InterpComp6->SetupAttachment(GetCameraComponent());
	/////////////////////////////////////////////////////////////////////////////////////

	FootstepComponent = CreateDefaultSubobject<UTW_FootstepComponent>(TEXT("FootstepComp"));
}

void ATW_BaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	check(TWCharacterManager)
	check(TWCameraManager)
	check(WeaponManager)
	check(CrosshairAimFactorComponent)
	check(GetCharacterMovement())
	check(TWInventoryComp)
	check(HealthComponent)

	GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;

	HealthComponent->OnDeath.AddUObject(this, &ATW_BaseCharacter::OnDeathHandle);
	InitializeInterpLocations();
}

void ATW_BaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	CalculateCrosshairSpread(DeltaTime);
	TraceForItem();
	InterpCapsuleHalfHeight(DeltaTime);

	const auto Health = HealthComponent->GetHealth();
	TextHealthComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));
}

void ATW_BaseCharacter::Jump()
{
	if (bCrouching)
	{
		bCrouching = false;
		GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;
	}
	else
	{
		ACharacter::Jump();
	}
}

void ATW_BaseCharacter::InterpCapsuleHalfHeight(float DeltaTime)
{
	float TargetCapsuleHalfHeight;

	if (bCrouching)
	{
		TargetCapsuleHalfHeight  = this->CrouchingCapsuleHalfHeight;
	}
	else
	{
		TargetCapsuleHalfHeight = this->StandingCapsuleHalfHeight;
	}

	const float InterpHalfHeight = FMath::FInterpTo(GetCapsuleComponent()->GetScaledCapsuleHalfHeight(),
													TargetCapsuleHalfHeight, DeltaTime, 20.f);
	const float DeltaCapsuleHalfHeight = InterpHalfHeight - GetCapsuleComponent()->GetScaledCapsuleHalfHeight();
	const auto MeshOffset = FVector(0.f, 0.f, -DeltaCapsuleHalfHeight);
	GetMesh()->AddLocalOffset(MeshOffset);
	GetCapsuleComponent()->SetCapsuleHalfHeight(InterpHalfHeight);
}

void ATW_BaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent)

	PlayerInputComponent->BindAxis("MoveForward", this, &ATW_BaseCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ATW_BaseCharacter::MoveRight);
	PlayerInputComponent->BindAxis("TurnRate", this, &ATW_BaseCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ATW_BaseCharacter::LookUpAtRate);
	PlayerInputComponent->BindAxis("Turn", this, &ATW_BaseCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("Lookup", this, &ATW_BaseCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ATW_BaseCharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("FireButton", IE_Pressed, this, &ATW_BaseCharacter::FireButtonPressed);
	PlayerInputComponent->BindAction("FireButton", IE_Released, this, &ATW_BaseCharacter::FireButtonReleased);
	
	PlayerInputComponent->BindAction("AimingButton", IE_Pressed, this, &ATW_BaseCharacter::AimingButtonPressed);
	PlayerInputComponent->BindAction("AimingButton", IE_Released, this, &ATW_BaseCharacter::AimingButtonReleased);

	PlayerInputComponent->BindAction("Select", IE_Pressed, this, &ATW_BaseCharacter::SelectButtonPressed);
	PlayerInputComponent->BindAction("ReloadButton", IE_Pressed, this, &ATW_BaseCharacter::ReloadButtonPressed);

	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ATW_BaseCharacter::CrouchButtonPressed);

	PlayerInputComponent->BindAction("FirstSlotInventory", IE_Pressed, this, &ATW_BaseCharacter::WeaponFirstButtonPressed);
	PlayerInputComponent->BindAction("SecondSlotInventory", IE_Pressed, this, &ATW_BaseCharacter::WeaponSecondButtonPressed);
	PlayerInputComponent->BindAction("ThirdSlotInventory", IE_Pressed, this, &ATW_BaseCharacter::WeaponThirdButtonPressed);
	PlayerInputComponent->BindAction("FourSlotInventory", IE_Pressed, this, &ATW_BaseCharacter::WeaponFourButtonPressed);
}

void ATW_BaseCharacter::MoveForward(float Value)
{
	if (Controller && Value != 0.f)
	{
		const FRotator Rotation{ Controller->GetControlRotation() };
		const FRotator YawRotation{ 0.f, Rotation.Yaw, 0.f };

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
	}
	
}

void ATW_BaseCharacter::MoveRight(float Value)
{
	if (Controller && Value != 0.f)
	{
		const FRotator Rotation{ Controller->GetControlRotation() };
		const FRotator YawRotation{ 0.f, Rotation.Yaw, 0.f };

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ATW_BaseCharacter::TurnAtRate(float Rate)
{
	float ZoomScaleFactor = 1;
	if (TWCameraManager->bAiming)
	{
		ZoomScaleFactor *= TWCameraManager->ZoomTurnRate;
	}

	// deg/sec * sec/frame
	AddControllerYawInput(Rate * ZoomScaleFactor * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ATW_BaseCharacter::LookUpAtRate(float Rate)
{
	float ZoomScaleFactor = 1;
	if (TWCameraManager->bAiming)
	{
		ZoomScaleFactor *= TWCameraManager->ZoomLookUpRate;
	}
	
	// deg/sec * sec/frame
	AddControllerPitchInput(Rate * ZoomScaleFactor * BaseLookupRate * GetWorld()->GetDeltaSeconds());
}

void ATW_BaseCharacter::FireButtonPressed()
{
	bFireButtonPressed = true;
	WeaponManager->StartFire();
}

void ATW_BaseCharacter::FireButtonReleased()
{
	bFireButtonPressed = false;
	WeaponManager->StopFire();
}

void ATW_BaseCharacter::AimingButtonPressed()
{
	bAimingButtonPressed = true;
	if (WeaponManager->CurrentEquippedWeapon->GetCombatState() != ECombatState::ECS_Reloading &&
		WeaponManager->CurrentEquippedWeapon->GetCombatState() != ECombatState::ECS_Equipping)
	{
		StartAim();
	}
}

void ATW_BaseCharacter::AimingButtonReleased()
{
	bAimingButtonPressed = false;
	StopAim();
}

void ATW_BaseCharacter::StartAim()
{
	TWCameraManager->bAiming = true;
	GetCharacterMovement()->MaxWalkSpeed = CrouchMovementSpeed;
}

void ATW_BaseCharacter::StopAim()
{
	TWCameraManager->bAiming = false;
	if (!bCrouching)
	{
		GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;
	}
}

void ATW_BaseCharacter::CalculateCrosshairSpread(float DeltaTime) const
{
	FVector Velocity = GetVelocity();
	Velocity.Z = 0.f;

	CrosshairAimFactorComponent->Velocity = Velocity;
	CrosshairAimFactorComponent->bIsAiming = TWCameraManager->bAiming;
	CrosshairAimFactorComponent->bIsFalling = GetCharacterMovement()->IsFalling();
	CrosshairAimFactorComponent->CalculateCrosshairSpread(DeltaTime);
}

void ATW_BaseCharacter::ReloadButtonPressed()
{
	WeaponManager->ReloadCurrentAmmo();
}

void ATW_BaseCharacter::CrouchButtonPressed()
{
	if (!GetCharacterMovement()->IsFalling())
	{
		bCrouching = !bCrouching;
	}

	if (bCrouching)
	{
		GetCharacterMovement()->MaxWalkSpeed = CrouchMovementSpeed;
		GetCharacterMovement()->GroundFriction = CrouchingGroundFriction;
	}
	else
	{
		GetCharacterMovement()->MaxWalkSpeed = BaseMovementSpeed;
		GetCharacterMovement()->GroundFriction = BaseGroundFriction;
	}
}

void ATW_BaseCharacter::SelectButtonPressed()
{
	if (WeaponManager->CurrentEquippedWeapon->GetCombatState() != ECombatState::ECS_Unoccupied) return;
	if (TraceHitItem)
	{
		TraceHitItem->StartItemCurve(this);
		TraceHitItem = nullptr;
		TraceHitItemLastFrame = nullptr;
	}
	
}

bool ATW_BaseCharacter::TraceUnderCrosshair(FHitResult& OutHitResult, FVector& OutHitLocation) const
{
	FVector2D ViewportSize;
	if (GEngine && GEngine->GameViewport)
	{
		GEngine->GameViewport->GetViewportSize(ViewportSize);
	}

	// Get screen space location of crosshairs
	const FVector2D CrosshairLocation = FVector2D(ViewportSize.X / 2.f, ViewportSize.Y / 2.f);
	FVector CrosshairWorldPosition;
	FVector CrosshairWorldDirection;

	const bool bScreenToWorld = UGameplayStatics::DeprojectScreenToWorld(
		UGameplayStatics::GetPlayerController(this, 0),
		CrosshairLocation, CrosshairWorldPosition, CrosshairWorldDirection);

	if (bScreenToWorld)
	{
		const FVector Start = CrosshairWorldPosition;
		const FVector End = Start + CrosshairWorldDirection * TraceShotLenght;
		OutHitLocation = End;

		GetWorld()->LineTraceSingleByChannel(OutHitResult, Start, End, ECC_Visibility);

		if (OutHitResult.bBlockingHit)
		{
			OutHitLocation = OutHitResult.Location;
			return true;
		}
	}
	return false;
}

void ATW_BaseCharacter::MakeHighlightInventoryIcon(const ATW_BaseWeapon* TraceHitWeapon)
{
	if (TraceHitWeapon)
	{
		if (WeaponManager->HighlightedSlot == -1)
		{
			HighlightInventorySlot();
		}
	}
	else
	{
		if (WeaponManager->HighlightedSlot != -1)
		{
			UnHighlightInventorySlot();
		}
	}
}

void ATW_BaseCharacter::FinishDeath()
{
	GetMesh()->bPauseAnims = true;
}

void ATW_BaseCharacter::OnDeathHandle(AController* InstigatedBy) 
{
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && DeathMontage)
	{
		AnimInstance->Montage_Play(DeathMontage);
		AnimInstance->Montage_JumpToSection(FName("DeathA"), DeathMontage);
	}

	APlayerController* PC = UGameplayStatics::GetPlayerController(this, 0);
	if (PC)
	{
		DisableInput(PC);
	}
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);

	auto* EnemyController = Cast<ATW_EnemyAIController>(InstigatedBy);
	if (EnemyController)
	{
		EnemyController->GetBlackboardComponent()->SetValueAsBool(
			FName(TEXT("CharacterDead")),
			true
		);
	}
	//SetLifeSpan(5.f);
}

void ATW_BaseCharacter::TraceForItem()
{
	if (bShouldTraceForItems)
	{
		FHitResult ItemTraceResult;
		FVector HitLocation;

		TraceUnderCrosshair(ItemTraceResult, HitLocation);

		if (ItemTraceResult.bBlockingHit)
		{
			TraceHitItem = Cast<ATW_BaseItem>(ItemTraceResult.Actor);

			const auto TraceHitWeapon = Cast<ATW_BaseWeapon>(TraceHitItem);
			if (TraceHitWeapon)
			{
				MakeHighlightInventoryIcon(TraceHitWeapon);
			}
			
			if (TraceHitItem && TraceHitItem->GetItemState() == EItemState::EIS_EquipInterping)
			{
				TraceHitItem = nullptr;
			}

			if (TraceHitItem && TraceHitItem->GetPickupWidget())
			{
				TraceHitItem->GetPickupWidget()->SetVisibility(true);
				TraceHitItem->EnableCustomDepth();
			}

			if (TraceHitItemLastFrame)
			{
				if (TraceHitItem != TraceHitItemLastFrame)
				{
					TraceHitItemLastFrame->GetPickupWidget()->SetVisibility(false);
					TraceHitItemLastFrame->DisableCustomDepth();
				}
			}

			TraceHitItemLastFrame = TraceHitItem;
		}
	}
	else if (TraceHitItemLastFrame)
	{
		TraceHitItemLastFrame->GetPickupWidget()->SetVisibility(false);
		TraceHitItemLastFrame->DisableCustomDepth();
	}
}

bool ATW_BaseCharacter::GetAiming() const
{
	return TWCameraManager->bAiming;
}

void ATW_BaseCharacter::AddOverlappedItemCount(int8 Amount)
{
	if (OverlappedItemCount + Amount <= 0)
	{
		OverlappedItemCount = 0;
		bShouldTraceForItems = false;
	}
	else
	{
		OverlappedItemCount += Amount;
		bShouldTraceForItems = true;
	}
}

void ATW_BaseCharacter::GetPickupItem(ATW_BaseItem* Item)
{
	// TODO: Rename Function and Add return bool
	
	const auto Weapon = Cast<ATW_BaseWeapon>(Item);
	if (Weapon)
	{
		if (TWInventoryComp->IsHaveFreeSpace())
		{
			TWInventoryComp->AddWeaponToInventory(Weapon);
		}
		else
		{
			WeaponManager->SwapWeapon(Weapon);
		}
		TraceHitItem = nullptr;
		TraceHitItemLastFrame = nullptr;
	}

	const auto Ammo = Cast<ATW_BaseAmmo>(Item);

	if (Ammo)
	{
		WeaponManager->PickupAmmo(Ammo);
	}
	
}

void ATW_BaseCharacter::InitializeInterpLocations()
{
	FInterpLocation WeaponLocation{ WeaponInterpComp, 0 };
	TWCameraManager->AddInterpLocations(WeaponLocation);

	FInterpLocation InterpLoc1{ InterpComp1, 0 };
	TWCameraManager->AddInterpLocations(InterpLoc1);

	FInterpLocation InterpLoc2{ InterpComp2, 0 };
	TWCameraManager->AddInterpLocations(InterpLoc2);

	FInterpLocation InterpLoc3{ InterpComp3, 0 };
	TWCameraManager->AddInterpLocations(InterpLoc3);

	FInterpLocation InterpLoc4{ InterpComp4, 0 };
	TWCameraManager->AddInterpLocations(InterpLoc4);

	FInterpLocation InterpLoc5{ InterpComp5, 0 };
	TWCameraManager->AddInterpLocations(InterpLoc5);

	FInterpLocation InterpLoc6{ InterpComp6, 0 };
	TWCameraManager->AddInterpLocations(InterpLoc6);
}

void ATW_BaseCharacter::HighlightInventorySlot()
{
	const int32 EmptySlot{ TWInventoryComp->GetEmptyInventorySlot() };
	WeaponManager->HighlightIconDelegate.Broadcast(EmptySlot, true);
	WeaponManager->HighlightedSlot = EmptySlot;	
}

void ATW_BaseCharacter::UnHighlightInventorySlot()
{
	WeaponManager->HighlightIconDelegate.Broadcast(WeaponManager->HighlightedSlot, false);
	WeaponManager->HighlightedSlot = -1;
}

void ATW_BaseCharacter::WeaponFirstButtonPressed()
{
	if (WeaponManager->CurrentEquippedWeapon->SlotIndex == 0) return;
	TWInventoryComp->ExchangeInventoryWeapon(0);
}

void ATW_BaseCharacter::WeaponSecondButtonPressed()
{
	if (WeaponManager->CurrentEquippedWeapon->SlotIndex == 1) return;
	TWInventoryComp->ExchangeInventoryWeapon(1);
}

void ATW_BaseCharacter::WeaponThirdButtonPressed()
{
	if (WeaponManager->CurrentEquippedWeapon->SlotIndex == 2) return;
	TWInventoryComp->ExchangeInventoryWeapon(2);
}

void ATW_BaseCharacter::WeaponFourButtonPressed()
{
	if (WeaponManager->CurrentEquippedWeapon->SlotIndex == 3) return;
	TWInventoryComp->ExchangeInventoryWeapon(3);
}
