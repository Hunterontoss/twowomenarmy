#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "TW_EnemyAIController.generated.h"

class UBlackboardComponent;
class UBehaviorTreeComponent;
/**
 * 
 */
UCLASS()
class TW_TWOWOMENARMY_API ATW_EnemyAIController : public AAIController
{
	GENERATED_BODY()
public:
	ATW_EnemyAIController();
	virtual void OnPossess(APawn* InPawn) override;

protected:
	virtual void BeginPlay() override;
	/////////////////////////////////////////////////////////////////////////////////////
	/// Blackboard
	///

protected:
	UPROPERTY(BlueprintReadWrite, Category = "AI Behavior")
	UBlackboardComponent* BlackboardComponent;
	
	UPROPERTY(BlueprintReadWrite, Category = "AI Behavior")
	UBehaviorTreeComponent* BehaviorTreeComponent;

	/////////////////////////////////////////////////////////////////////////////////////
};
