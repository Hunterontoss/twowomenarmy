#include "TWComponents/Combat/TW_HitReactionComponent.h"
#include "GameFramework/Character.h"

DEFINE_LOG_CATEGORY_STATIC(LogHitReactionMontage, All, All)

UTW_HitReactionComponent::UTW_HitReactionComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UTW_HitReactionComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTW_HitReactionComponent::PlayHitMontage(FName Section, float PlayRate)
{
	const auto* L_Owner = GetOwner<ACharacter>();
	if (L_Owner)
	{
		if (bCanHitReact)
		{
			auto* AnimInstance = L_Owner->GetMesh()->GetAnimInstance();
			if (AnimInstance)
			{
				AnimInstance->Montage_Play(HitMontage, PlayRate);
				AnimInstance->Montage_JumpToSection(Section, HitMontage);
			}
			else
			{
				UE_LOG(LogHitReactionMontage, Warning, TEXT("AnimInstance Is Null!"))
			}

			bCanHitReact = false;

			const float L_HitReactTime = FMath::FRandRange(HitReactTimeMin, HitReactTimeMax);
			GetWorld()->GetTimerManager().SetTimer(
				HitReactTimer,
				this,
				&UTW_HitReactionComponent::ResetHitReactTimer,
				L_HitReactTime
				);
		}
	}
	else
	{
		UE_LOG(LogHitReactionMontage, Warning, TEXT("Owner Is Null!"))
	}
}

void UTW_HitReactionComponent::ResetHitReactTimer()
{
	bCanHitReact = true;
}

