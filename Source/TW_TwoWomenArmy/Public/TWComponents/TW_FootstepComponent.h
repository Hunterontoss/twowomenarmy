#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_FootstepComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_FootstepComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_FootstepComponent();

protected:
	virtual void BeginPlay() override;

public:
	UFUNCTION(BlueprintCallable)
	EPhysicalSurface GetSurfaceType();

private:
	UPROPERTY(VisibleDefaultsOnly)
	AActor* Owner;
};
