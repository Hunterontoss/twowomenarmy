#pragma once
#include "TW_CoreTypes.generated.h"

class ATW_BaseCharacter;

// ============================ WEAPON ============================
class ATW_BaseWeapon;

UENUM(BlueprintType)
enum class  EAmmoType : uint8
{
	EAT_9mm UMETA(DisplayName = "9mm"),
	EAT_556mm UMETA(DisplayName = "5.56mm"),
	EAT_MAX UMETA(DisplayName = "DefaultMax")
};