#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TWItems/TW_BaseWeapon.h"
#include "TW_BaseCharacter.generated.h"


class UTextRenderComponent;
class UTW_HealthComponent;
class UTW_ShooterCharacterManager;
class UTW_CameraManagerComponent;
class UCrosshairAimFactorComponent;
class USpringArmComponent;
class UCameraComponent;
class UAnimMontage;
class ATW_BaseItem;
class UTW_WeaponManagerComponent;
class UTW_InventoryComponent;
class UTW_FootstepComponent;

UCLASS()
class TW_TWOWOMENARMY_API ATW_BaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATW_BaseCharacter();
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	FORCEINLINE USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	FORCEINLINE UCameraComponent* GetCameraComponent() const { return FollowCamera; }
	FORCEINLINE bool GetAiming() const;  
	FORCEINLINE bool GetFireButtonPressed() const { return bFireButtonPressed;}
	FORCEINLINE bool GetAimingButtonPressed() const { return bAimingButtonPressed; }
	FORCEINLINE bool GetCrouching() const { return bCrouching; }
	FORCEINLINE int8 GetOverlappedItemCount() const { return OverlappedItemCount;}
	
	void AddOverlappedItemCount(int8 Amount);
	void GetPickupItem(ATW_BaseItem* Item);

	/////////////////////////////////////////////////////////////////////////////////////
	/// Components
	///
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Manager)
	UTW_WeaponManagerComponent* WeaponManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Manager)
	UTW_CameraManagerComponent* TWCameraManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Manager)
	UTW_ShooterCharacterManager* TWCharacterManager;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
	UTW_InventoryComponent* TWInventoryComp;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat)
	USceneComponent* HandSceneComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_FootstepComponent* FootstepComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_HealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTextRenderComponent* TextHealthComponent;

	/////////////////////////////////////////////////////////////////////////////////////
	
	bool TraceUnderCrosshair(FHitResult& OutHitResult, FVector& OutHitLocation) const;
	
protected:
	virtual void BeginPlay() override;
	virtual void Jump() override;

	void ReloadButtonPressed();
	void CrouchButtonPressed();
	/////////////////////////////////////////////////////////////////////////////////////
	/// Movement
	///
	void MoveForward(float Value);
	void MoveRight(float Value);
	void TurnAtRate(float Rate);
	void LookUpAtRate(float Rate);

	
	/////////////////////////////////////////////////////////////////////////////////////
	/// FireWeapon
	///
	void FireButtonPressed();
	void FireButtonReleased();
	

	/////////////////////////////////////////////////////////////////////////////////////
	/// Aim
	///

public:
	void StartAim();
	void StopAim();

protected:
	void AimingButtonPressed();
	void AimingButtonReleased();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Components)
	UCrosshairAimFactorComponent* CrosshairAimFactorComponent;
	
	void TraceForItem();
private:

	bool bAimingButtonPressed;
	
	void CalculateCrosshairSpread(float DeltaTime) const;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USpringArmComponent* CameraBoom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	UCameraComponent* FollowCamera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float BaseTurnRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	float BaseLookupRate;
			
	float TraceShotLenght{300000.f};

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	ATW_BaseItem* TraceHitItem;
	/////////////////////////////////////////////////////////////////////////////////////
	/// FIRE
	///
	bool bFireButtonPressed = false;
	/////////////////////////////////////////////////////////////////////////////////////

	bool bShouldTraceForItems;
	int8 OverlappedItemCount;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Items, meta=(AllowPrivateAccess=true))
	ATW_BaseItem* TraceHitItemLastFrame;

	void SelectButtonPressed();

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bCrouching;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Movement 
	///

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float BaseMovementSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float CrouchMovementSpeed;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Capsule Half Height 
	///

protected:
	void InterpCapsuleHalfHeight(float DeltaTime);

private:
	float CurrentCapsuleHalfHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float StandingCapsuleHalfHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float CrouchingCapsuleHalfHeight;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float BaseGroundFriction;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float CrouchingGroundFriction;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Pickup Interpolation
	///
	
protected:
	void InitializeInterpLocations();
	
private:
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* WeaponInterpComp;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* InterpComp1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* InterpComp2;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* InterpComp3;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* InterpComp4;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* InterpComp5;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
	USceneComponent* InterpComp6;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Inventory
	///
protected:

	void WeaponFirstButtonPressed();
	void WeaponSecondButtonPressed();
	void WeaponThirdButtonPressed();
	void WeaponFourButtonPressed();
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Highlight Icon Inventory
	///
	
public:
	void HighlightInventorySlot();
	void UnHighlightInventorySlot();

private:
	void MakeHighlightInventoryIcon(const ATW_BaseWeapon* TraceHitWeapon);

	/////////////////////////////////////////////////////////////////////////////////////
	///  Death
	///
public:
	UFUNCTION(BlueprintCallable)
	void FinishDeath();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UAnimMontage* DeathMontage;

private:
	UFUNCTION()
	void OnDeathHandle(AController* InstigatedBy);
};
