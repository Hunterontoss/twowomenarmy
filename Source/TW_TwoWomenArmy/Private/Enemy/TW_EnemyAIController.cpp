// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy/TW_EnemyAIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Enemy/TW_BaseEnemy.h"
#include "Enemy/AI/TW_MeleeAIComponent.h"

DEFINE_LOG_CATEGORY_STATIC(LogEnemyAIController, All, All)

ATW_EnemyAIController::ATW_EnemyAIController()
{
	BlackboardComponent = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	BehaviorTreeComponent = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BEhaviorTreeComp"));
}

void ATW_EnemyAIController::BeginPlay()
{
	Super::BeginPlay();
	check(BlackboardComponent)
	check(BehaviorTreeComponent)
}


void ATW_EnemyAIController::OnPossess(APawn* InPawn)
{
	Super::OnPossess(InPawn);
	if (!InPawn) return;

	const auto* Enemy = Cast<ATW_BaseEnemy>(InPawn);
	
	if (Enemy)
	{
		const auto* MeleeAIComp = Enemy->FindComponentByClass<UTW_MeleeAIComponent>();	
		
		if (MeleeAIComp && MeleeAIComp->GetBehaviorTree())
		{
			BlackboardComponent->InitializeBlackboard(*(MeleeAIComp->GetBehaviorTree()->BlackboardAsset));
		}
		else
		{
			UE_LOG(LogEnemyAIController, Warning, TEXT("MeleeAIComp not found!"));
		}
	}
	else
	{
		UE_LOG(LogEnemyAIController, Warning, TEXT("Enemy Class not found!"))
	}
}

