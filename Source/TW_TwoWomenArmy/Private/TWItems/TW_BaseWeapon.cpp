// Fill out your copyright notice in the Description page of Project Settings.


#include "TWItems/TW_BaseWeapon.h"

#include "Enemy/TW_BaseEnemy.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystemComponent.h"
#include "Players/TW_BaseCharacter.h"
#include "Sound/SoundCue.h"
#include "TWInterfaces/TW_BulletHitInterface.h"
#include "TWItems/TW_ItemEffectsComponent.h"

ATW_BaseWeapon::ATW_BaseWeapon() : ThrowWeaponTime(0.6f), bFalling(false), CurrentAmmo(30),
   MagazineCapacity(30), AmmoType(EAmmoType::EAT_9mm), MuzzleSocketName(FName("BarrelSocket")),
   TracerTargetPoint(FName("Target")), FireRate(600.f),
   CombatState(ECombatState::ECS_Unoccupied), ReloadMontageSection(FName(TEXT("Reload SMG"))),
   ClipBoneName(TEXT("smg_clip")), WeaponType(EWeaponType::EWT_SubmachineGun)
{
	
}

void ATW_BaseWeapon::BeginPlay()
{
	Super::BeginPlay();
	
	UpdateFireRate();

	if (BoneToHide != FName(""))
	{
		GetItemMesh()->HideBoneByName(BoneToHide, EPhysBodyOp::PBO_None);
	}
}

void ATW_BaseWeapon::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	
	const FString WeaponTablePath{ TEXT("DataTable'/Game/_TwoWomenArmy/DataTables/WeaponDatatable.WeaponDatatable'") };
	const UDataTable* WeaponTableObject = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *WeaponTablePath));
	if (WeaponTableObject)
	{
		FWeaponDataTable* WeaponDataRow = nullptr;
		switch (WeaponType)
		{
		case EWeaponType::EWT_SubmachineGun:
			WeaponDataRow = WeaponTableObject->FindRow<FWeaponDataTable>(FName("SubmachineGun"), TEXT(""));
			break;
		case EWeaponType::EWT_AssaultRifle:
			WeaponDataRow = WeaponTableObject->FindRow<FWeaponDataTable>(FName("AssaultRifle"), TEXT(""));
			break;
		case EWeaponType::EWT_Pistol:
			WeaponDataRow = WeaponTableObject->FindRow<FWeaponDataTable>(FName("Pistol"), TEXT(""));
		default:
			break;
		}

		if (WeaponDataRow)
		{
			InitVariableFromTable(*WeaponDataRow);	
		}

		if (MaterialInstance)
		{
			DynamicMaterialInstance = UMaterialInstanceDynamic::Create(MaterialInstance, this);
			DynamicMaterialInstance->SetVectorParameterValue(TEXT("FresnelColor"), GlowColor);
			GetItemMesh()->SetMaterial(MaterialIndex, DynamicMaterialInstance);
			
			EnableGlowMaterial();
		}
	}
}

void ATW_BaseWeapon::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	if (GetItemState() == EItemState::EIS_Falling && bFalling)
	{
		KeepWeaponUpright();
	}

	// Pistol Slide 
	UpdateSlideDisplacement();
}

void ATW_BaseWeapon::InitVariableFromTable(const FWeaponDataTable& DataTable)
{
	AmmoType = DataTable.AmmoType;
	CurrentAmmo = DataTable.WeaponAmmo;
	MagazineCapacity = DataTable.MagazingCapacity;
	PickupSound = DataTable.PickupSound;
	EquipSound = DataTable.EquipSound;
	GetItemMesh()->SetSkeletalMesh(DataTable.ItemMesh);
	ItemName = (DataTable.ItemName);
	IconItem = (DataTable.InventoryIcon);
	IconAmmo = (DataTable.AmmoIcon);
	MaterialInstance = (DataTable.MaterialInstance);
	PreviousMaterialIndex = MaterialIndex;
	GetItemMesh()->SetMaterial(PreviousMaterialIndex, nullptr);
	MaterialIndex = DataTable.MaterialIndex;
	ClipBoneName = DataTable.ClipBoneName;
	ReloadMontageSection = DataTable.ReloadMontageSection;
	ItemMesh->SetAnimInstanceClass(DataTable.AnimationBP);
	
	CrosshairsMiddle = DataTable.CrosshairsMiddle;
	CrosshairsLeft = DataTable.CrosshairsLeft;
	CrosshairsRight = DataTable.CrosshairsRight;
	CrosshairsTop = DataTable.CrosshairsTop;
	CrosshairsBottom = DataTable.CrosshairsBottom;

	////////// TODO: Make SetFireRate(); 
	FireRate = DataTable.AutoFireRate;
	UpdateFireRate();
	///////////////////////////////////
	
	MuzzleFlashParticle = DataTable.MuzzleFlash;
	FireSound = DataTable.FireSound;

	BoneToHide = DataTable.BoneToHide;
	bAutomatic = DataTable.bAutomatic;

	BaseDamage = DataTable.Damage;
	HeadShotDamage = DataTable.HeadShotDamage;
}

// Pistol
void ATW_BaseWeapon::StartSlideTimer()
{
	bMovingSlide = true;

	GetWorldTimerManager().SetTimer(SlideTimer, this, &ATW_BaseWeapon::FinishMovingSlide, SlideDisplacementTime);
}

void ATW_BaseWeapon::FinishMovingSlide()
{
	bMovingSlide = false;
}

void ATW_BaseWeapon::UpdateSlideDisplacement()
{
	if (SlideDisplacementCurve && bMovingSlide)
	{
		const float ElapsedTime = GetWorldTimerManager().GetTimerElapsed(SlideTimer);
		const float CurveValue = SlideDisplacementCurve->GetFloatValue(ElapsedTime);

		SlideDisplacement = CurveValue * MaxSlideDisplacement;
		RecoilRotation = CurveValue * MaxRecoilRotation;
	}
}
/// End Pistol 
///////////////////////////////////

void ATW_BaseWeapon::Fire()
{
	if (CombatState != ECombatState::ECS_Unoccupied) return;
	
	if (CurrentAmmo > 0)
	{
		SendBullet();
		PlayFireSound();
		PlayGunfireMontage();
		DecrementAmmo();

		// Start moving slide timer
		if (GetWeaponType() == EWeaponType::EWT_Pistol)
		{
			StartSlideTimer();
		}
		///////////////////////////////////
	}
}
void ATW_BaseWeapon::ThrowWeapon()
{
	KeepWeaponUpright();
	
	const FVector MeshForward = GetItemMesh()->GetForwardVector();
	const FVector MeshRight = GetItemMesh()->GetRightVector();

	FVector ImpulseDirection = MeshRight.RotateAngleAxis(-20.f, MeshForward);

	const float RandomRotation = FMath::RandRange(-60.f, 60.f);

	ImpulseDirection = ImpulseDirection.RotateAngleAxis(RandomRotation, FVector(0.f, 0.f, 1.f));
	ImpulseDirection*= 15'000.f;
	GetItemMesh()->AddImpulse(ImpulseDirection);

	bFalling = true;
	GetWorldTimerManager().SetTimer(
		ThrowWeaponTimer, this, &ATW_BaseWeapon::StopFalling, ThrowWeaponTime);

	EnableGlowMaterial();
}

void ATW_BaseWeapon::StopFalling()
{
	bFalling = false;
	SetItemState(EItemState::EIS_Pickup);
	ItemEffectsComp->StartPulseTimer();
}

void ATW_BaseWeapon::KeepWeaponUpright()
{
	const FRotator MeshRotation = FRotator(0.f, GetItemMesh()->GetComponentRotation().Yaw, 0.f);
	GetItemMesh()->SetWorldRotation(MeshRotation, false, nullptr, ETeleportType::TeleportPhysics);
}

void ATW_BaseWeapon::DecrementAmmo()
{
	if (CurrentAmmo - 1 <= 0)
	{
		CurrentAmmo = 0;
	}
	else
	{
		CurrentAmmo-=1;
	}
}


void ATW_BaseWeapon::StartReload()
{
	if (CombatState != ECombatState::ECS_Unoccupied) return;
	CombatState = ECombatState::ECS_Reloading;
	
	auto* BaseCharacter = Cast<ATW_BaseCharacter>(GetOwner());

	if (BaseCharacter->GetAiming())
	{
		BaseCharacter->StopAim();
	}
	
	if (BaseCharacter)
	{
		UAnimInstance* AnimInstance = BaseCharacter->GetMesh()->GetAnimInstance();
		if (AnimInstance && ReloadMontage)
		{
			AnimInstance->Montage_Play(ReloadMontage);
			AnimInstance->Montage_JumpToSection(GetReloadMontageSection());
		}	
	}
}

void ATW_BaseWeapon::ReloadAmmo(int32 CountAmmoToAdd)
{
	checkf(CurrentAmmo + CountAmmoToAdd <= MagazineCapacity, TEXT("Attempted to Reload. AmmoToAdd > MagazineCapacity "))
	CurrentAmmo += CountAmmoToAdd;
}

bool ATW_BaseWeapon::ClipIsFull() const
{
	return CurrentAmmo < MagazineCapacity;
}

void ATW_BaseWeapon::InitializeCustomDepth()
{
	EnableCustomDepth();
}

void ATW_BaseWeapon::StartEquip()
{
	CombatState = ECombatState::ECS_Equipping;

	const auto* BaseCharacter = Cast<ATW_BaseCharacter>(GetOwner());
	if (BaseCharacter)
	{
		UAnimInstance* AnimInstance = BaseCharacter->GetMesh()->GetAnimInstance();
		if (AnimInstance && EquipMontage)
		{
			AnimInstance->Montage_Play(EquipMontage, 1.0f);
			AnimInstance->Montage_JumpToSection(FName("Equip"));
		}	
	}
}

void ATW_BaseWeapon::PlayFireSound()
{
	if (FireSound)
	{
		UGameplayStatics::PlaySound2D(this, FireSound);	
	}
}

void ATW_BaseWeapon::PlayFireEffect(FVector TraceEnd)
{
	if (MuzzleFlashParticle)
	{
		UGameplayStatics::SpawnEmitterAttached(MuzzleFlashParticle, GetItemMesh(), MuzzleSocketName);
	}

	if (BeamParticle)
	{
		const FVector MuzzleLocation = GetItemMesh()->GetSocketLocation(MuzzleSocketName);
		const auto ParticleComponent = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), BeamParticle, MuzzleLocation);
		if (ParticleComponent)
		{
			ParticleComponent->SetVectorParameter(TracerTargetPoint, TraceEnd);
		}
	}
}

void ATW_BaseWeapon::SendBullet()
{
	const auto* BaseCharacter = Cast<ATW_BaseCharacter>(GetOwner());
	
	if (BaseCharacter)
	{
		const FVector StartLocation = GetItemMesh()->GetSocketLocation(MuzzleSocketName);

		FHitResult HitResult;
		FVector TracerEndPoint;
		
		BaseCharacter->TraceUnderCrosshair(HitResult, TracerEndPoint);
		
		
		const bool Result = GetWorld()->LineTraceSingleByChannel(HitResult, StartLocation, TracerEndPoint, ECollisionChannel::ECC_Visibility);

		if (Result)
		{
			////// Hit //////
			if (HitResult.GetActor())
			{
				SetImpactFX(HitResult);
				MakeDamage(HitResult, BaseCharacter->GetController());
			}
			
			TracerEndPoint = HitResult.ImpactPoint;
		}

		PlayFireEffect(TracerEndPoint);
	}
}

void ATW_BaseWeapon::MakeDamage(const FHitResult HitResult, AController* Controller)
{
	const ATW_BaseEnemy* HitEnemy = Cast<ATW_BaseEnemy>(HitResult.GetActor());
	
	if (HitEnemy && HitResult.BoneName.ToString() == HitEnemy->GetHeadBoneName())
	{
		UGameplayStatics::ApplyDamage(
						HitResult.GetActor(),
						HeadShotDamage,
						Controller,
						this,
						UDamageType::StaticClass()
						);
	}
	else
	{
		UGameplayStatics::ApplyDamage(
						HitResult.GetActor(),
						BaseDamage,
						Controller,
						this,
						UDamageType::StaticClass()
						);	
	}
}

void ATW_BaseWeapon::UpdateFireRate()
{
	if (FireRate != 0)
	{
		TimeBetweenShot = 60 / FireRate;	
	}
}

void ATW_BaseWeapon::PlayGunfireMontage() const
{
	const auto* BaseCharacter = Cast<ATW_BaseCharacter>(GetOwner());

	if (BaseCharacter)
	{
		UAnimInstance* AnimInstance = BaseCharacter->GetMesh()->GetAnimInstance();
		if (AnimInstance && HipFireAnimMontage)
		{
			AnimInstance->Montage_Play(HipFireAnimMontage);
			AnimInstance->Montage_JumpToSection(FName("StartFire"));
		}	
	}
}

void ATW_BaseWeapon::SetImpactFX(const FHitResult HitResult) const
{
	ITW_BulletHitInterface* BulletHitInterface = Cast<ITW_BulletHitInterface>(HitResult.GetActor());
	if (BulletHitInterface)
	{
		BulletHitInterface->BulletHit_Implementation(HitResult);
	}
	else
	{
		if (ImpactParticles)
		{
			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				ImpactParticles,
				HitResult.Location);
		}
	}
}
