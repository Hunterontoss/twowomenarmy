#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_HitReactionComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_HitReactionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_HitReactionComponent();

protected:
	virtual void BeginPlay() override;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Hit Reaction 
	///
	
public:
	void PlayHitMontage(FName Section, float PlayRate = 1.0f);
	
protected:

	void ResetHitReactTimer();
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* HitMontage;

	FTimerHandle HitReactTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float HitReactTimeMin = 0.5f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	float HitReactTimeMax = 0.9f;

	bool bCanHitReact = true;
	/////////////////////////////////////////////////////////////////////////////////////
};
