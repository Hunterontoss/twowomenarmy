#include "TWComponents/TW_FootstepComponent.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "TW_TwoWomenArmy/TW_TwoWomenArmy.h"

UTW_FootstepComponent::UTW_FootstepComponent()
{
}
void UTW_FootstepComponent::BeginPlay()
{
	Super::BeginPlay();

	Owner = GetOwner<AActor>();
}

EPhysicalSurface UTW_FootstepComponent::GetSurfaceType()
{
	if (Owner)
	{
		FHitResult HitResult;
		const FVector Start = Owner->GetActorLocation();
		const FVector End = Start + FVector(0.f , 0.f, -400.f);
		FCollisionQueryParams QueryParams;
		QueryParams.bReturnPhysicalMaterial = true;

		GetWorld()->LineTraceSingleByChannel(HitResult, Start, End, ECollisionChannel::ECC_Visibility, QueryParams);

		return UPhysicalMaterial::DetermineSurfaceType(HitResult.PhysMaterial.Get());
	}
	return EPhysicalSurface::SurfaceType_Default;
}

