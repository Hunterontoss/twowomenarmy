#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TWInterfaces/TW_BulletHitInterface.h"
#include "TW_ExplosionBarrel.generated.h"

class UTW_EffectImpactBulletComponent;
class UTW_HealthComponent;

UCLASS()
class TW_TWOWOMENARMY_API ATW_ExplosionBarrel : public AActor, public ITW_BulletHitInterface
{
	GENERATED_BODY()
	
public:	
	ATW_ExplosionBarrel();
	
protected:
	virtual void BeginPlay() override;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Components
	///
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_EffectImpactBulletComponent* ImpactComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_HealthComponent* HealthComponent;
	/////////////////////////////////////////////////////////////////////////////////////
	
	virtual void BulletHit_Implementation(FHitResult HitResult) override;

protected:
	virtual void OnDeath(AController* InstigatedBy);
	virtual void OnHealthChanged(float Health, float HealthDelta);
};
