#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_CoreTypes.h"
#include "TW_WeaponManagerComponent.generated.h"

class ATW_BaseCharacter;
class ATW_BaseAmmo;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FEquipItemDelegate, int32, CurrentSlotIndex, int32, NewSlotIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FHighlightIconDelegate, int32, SlotIndex, bool, bStartAnimation);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_WeaponManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_WeaponManagerComponent();

	bool IsUnoccupied() const;

protected:
	virtual void BeginPlay() override;
	virtual void OnRegister() override;

public:	
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category=Combat)
	TSubclassOf<ATW_BaseWeapon> DefaultWeaponClass;
	
	void EquipWeapon(ATW_BaseWeapon* WeaponToEquip, bool bSwapping = false);
	void DropWeapon();
	void SwapWeapon(ATW_BaseWeapon* WeaponToSwap);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Combat)
	ATW_BaseWeapon* CurrentEquippedWeapon;

	UPROPERTY(BlueprintAssignable, Category = Delegates)
	FEquipItemDelegate EquipItemDelegate;
	
private:

	ATW_BaseWeapon* SpawnDefaultWeapon() const;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Fire
	///
public:
	
	void StartFire();
	void StopFire();

	bool IsFireTimerInProgress() const;
	
protected:
	void CurrentWeaponFire();
	
private:
	
	FTimerHandle TimerHandle_TimeBetweenShots;
	float LastTimeFire;

	//////////////////////////////////////////////////////////////////////////////////////////
	/// Ammo
	///

public:
	void PickupAmmo(ATW_BaseAmmo* Ammo);
	bool WeaponHasAmmo();
	void DecrementAmmo();
	void ReloadCurrentAmmo();
	bool IsReloading() const;
	bool IsEquipping() const;
	
	UFUNCTION(BlueprintCallable)
	void FinishReloading();

	UFUNCTION(BlueprintCallable)
	void FinishEquipping();
	
	UFUNCTION(BlueprintCallable)
	int GetAmmoCount();

	bool CarryingAmmo();
protected:
	void InitializeAmmoMap();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Combat, meta=(AllowPrivateAccess="true"))
	TMap<EAmmoType, int32> AmmoMap;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Combat, meta=(AllowPrivateAccess="true"))
	int32 Basic9mmAmmoCount;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Combat, meta=(AllowPrivateAccess="true"))
	int32 Basic556mmAmmoCount;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta=(AllowPrivateAccess="true"))
	ATW_BaseCharacter* OwnerCharacter;
	
	//////////////////////////////////////////////////////////////////////////////////////////

	/////////////////////////////////////////////////////////////////////////////////////
	/// Reload Animation
	///
public:
	UFUNCTION(BlueprintCallable)
	void GrabClip();

	UFUNCTION(BlueprintCallable)
	void ReleaseClip();

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	FTransform ClipTransform;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	USceneComponent* HandSceneComponent;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Highlight Empty Icon
	///
public:
	UPROPERTY(BlueprintAssignable, Category = Delegates)
	FHighlightIconDelegate HighlightIconDelegate;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
	int32 HighlightedSlot;
};
	