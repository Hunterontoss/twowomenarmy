// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_CameraManagerComponent.generated.h"

class ATW_BaseCharacter;
class UCameraComponent;

USTRUCT(BlueprintType)
struct FInterpLocation
{
	GENERATED_BODY()

	// Scene component to use for its location for interping
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	USceneComponent* SceneComponent;
	
	// Number of items interping to/at this scene comp location
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	int32 ItemCount;
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_CameraManagerComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_CameraManagerComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/////////////////////////////////////////////////////////////////////////////////////
	/// For Pickup 
	///

	FVector GetCameraInterpLocation();
	
	/** Distance outward from the camera for the interp destination */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	float CameraInterpDistance;

	/** Distance upward from the camera for the interp destination */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Items, meta = (AllowPrivateAccess = "true"))
	float CameraInterpElevation;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Aim
	///
public:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat)
	bool bAiming;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera,  meta = (ClampMin = "0.2", ClampMax = "1.0", UIMin = "0.2", UIMax = "1.0"))
	float ZoomTurnRate;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera,  meta = (ClampMin = "0.2", ClampMax = "1.0", UIMin = "0.2", UIMax = "1.0"))
	float ZoomLookUpRate;
	
protected:
	float CameraDefaultFOV;
	float CameraCurrentFOV;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Zoom)
	float CameraZoomedFOV;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = Zoom)
	float ZoomInterpSpeed;

	void CameraInterpZoom(float DeltaTime);
	/////////////////////////////////////////////////////////////////////////////////////

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Character)
	ATW_BaseCharacter* BaseCharacter;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Pickup Interpolation
	///
public:
	
	void AddInterpLocations(FInterpLocation Item);
	FInterpLocation GetInterpLocation(int32 Index);
	int32 GetInterpLocationIndex();
	void IncrementInterpLocItemCount(int32 Index, int32 Amount);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	TArray<FInterpLocation> InterpLocations;
};
