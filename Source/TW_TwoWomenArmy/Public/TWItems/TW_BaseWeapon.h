// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "TWItems/TW_BaseItem.h"
#include "TW_CoreTypes.h"
#include "TW_WeaponTypes.h"
#include "TW_BaseWeapon.generated.h"

class USoundCue;
class UParticleSystem;
class UAnimMontage;

UCLASS()
class TW_TWOWOMENARMY_API ATW_BaseWeapon : public ATW_BaseItem
{
	GENERATED_BODY()

public:
	ATW_BaseWeapon();
	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE EWeaponType GetWeaponType() const { return WeaponType; }
	FORCEINLINE EAmmoType GetAmmoType() const { return  AmmoType; }
	FORCEINLINE FName GetReloadMontageSection() const { return ReloadMontageSection; }
	FORCEINLINE int32 GetMagazineCapacity() const { return MagazineCapacity; }
	FORCEINLINE FName GetClipBoneName() const { return ClipBoneName; }
	FORCEINLINE ECombatState GetCombatState() const { return  CombatState; }
	FORCEINLINE float GetTimeBetweenShot() const { return TimeBetweenShot; }
	FORCEINLINE bool IsAutomatic() const { return bAutomatic; }
	
	FORCEINLINE void SetMovingClip(bool Move) { bMovingClip = Move; }
	void SetCombatState(const ECombatState NewCombatState) { CombatState = NewCombatState;}

	
protected:
	virtual void BeginPlay() override;

	virtual void OnConstruction(const FTransform& Transform) override;
	/////////////////////////////////////////////////////////////////////////////////////
	/// Throw Weapon 
	///

public:
	
	void ThrowWeapon();
	
protected:
	void StopFalling();

private:
	FTimerHandle ThrowWeaponTimer;
	float ThrowWeaponTime;
	bool bFalling;

	void KeepWeaponUpright();
	/////////////////////////////////////////////////////////////////////////////////////
	/// Ammo Weapon 
	///
public:
	FORCEINLINE int32 GetAmmo() const { return CurrentAmmo; }
	
	void DecrementAmmo();
private:
	/** Ammo count for this Weapon */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	int32 CurrentAmmo;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	int32 MagazineCapacity;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	EAmmoType AmmoType;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Effects Visual
	///
public:	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	USoundCue* FireSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	UParticleSystem* ImpactParticles;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	UParticleSystem* BeamParticle;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat)
	UParticleSystem* MuzzleFlashParticle;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FName MuzzleSocketName;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Weapon)
	FName TracerTargetPoint;
	
protected:
	
	void PlayFireSound();
	void PlayFireEffect(FVector TraceEnd);
	void PlayGunfireMontage() const;

private:
	void SetImpactFX(FHitResult HitResult) const;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// FIRE
	///
public:

	virtual void Fire();
	
protected:
	void SendBullet();
	void MakeDamage(const FHitResult HitResult, AController* Controller);
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon Properties")
	float BaseDamage = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties")
	float HeadShotDamage = 25.f;

	void UpdateFireRate();
	
private:

	/* RPM  - Bullets per minute fired by weapon */
	UPROPERTY(EditDefaultsOnly, Category=Weapon, meta = (AllowPrivateAccess = "true"))
	float FireRate;

	/* Derived from FireRate */
	float TimeBetweenShot;
	
	/** Combat State, can only fire or reload if Unoccupied */
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
    ECombatState CombatState;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* HipFireAnimMontage;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	bool bAutomatic = true;
	
	////////////////////////////////////////////////////////////////////////////////////////
	/// Reload 
	///
	
public:
	/** Handle reloading of the Weapon*/
	void StartReload();
	void ReloadAmmo(int32 CountAmmoToAdd);
	bool ClipIsFull() const;
	
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* ReloadMontage;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	FName ReloadMontageSection;

	/** True when moving the clip while reloading */	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	bool bMovingClip;

	/** Name for the clip bone */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	FName ClipBoneName;
	////////////////////////////////////////////////////////////////////////////////////////

protected:
	virtual void InitializeCustomDepth() override;
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon Properties", meta = (AllowPrivateAccess = "true"))
	EWeaponType WeaponType;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Equip  
	///
	
public:
	void StartEquip();

private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	UAnimMontage* EquipMontage;

	////////////////////////////////////////////////////////////////////////////////////////
	/// DataTable Weapon 
	///
protected:
	void InitVariableFromTable(const FWeaponDataTable& DataTable);
	
private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = DataTable, meta = (AllowPrivateAccess = "true"))
	UDataTable* WeaponDataTable;

	int32 PreviousMaterialIndex;

	////////////////////////////////////////////////////////////////////////////////////////
	/// Crosshair 
	///

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = DataTable, meta = (AllowPrivateAccess = "true"))
	UTexture2D* CrosshairsMiddle;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = DataTable, meta = (AllowPrivateAccess = "true"))
	UTexture2D* CrosshairsLeft;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = DataTable, meta = (AllowPrivateAccess = "true"))
	UTexture2D* CrosshairsRight;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = DataTable, meta = (AllowPrivateAccess = "true"))
	UTexture2D* CrosshairsBottom;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = DataTable, meta = (AllowPrivateAccess = "true"))
	UTexture2D* CrosshairsTop;
	////////////////////////////////////////////////////////////////////////////////////////
	///
	///

private:

	/** Name of the bone to hide on the weapon mesh */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = DataTable, meta = (AllowPrivateAccess = "true"))
	FName BoneToHide;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Pistol Weapon Slider 
	/// TODO: Make Class Pistol

public:
	void StartSlideTimer();
	
protected:
	void FinishMovingSlide();
	void UpdateSlideDisplacement();
	
private:
	// Slide
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pistol, meta = (AllowPrivateAccess = "true"))
	float SlideDisplacement = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pistol, meta = (AllowPrivateAccess = "true"))
	float SlideDisplacementTime = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pistol, meta = (AllowPrivateAccess = "true"))
	UCurveFloat* SlideDisplacementCurve;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pistol, meta = (AllowPrivateAccess = "true"))
	bool bMovingSlide = false;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pistol, meta = (AllowPrivateAccess = "true"))
	float MaxSlideDisplacement = 4.f;

	FTimerHandle SlideTimer;
	
	// Recoil
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Pistol, meta = (AllowPrivateAccess = "true"))
	float MaxRecoilRotation = 20.f;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Pistol, meta = (AllowPrivateAccess = "true"))
	float RecoilRotation;
	
	
	/////////////////////////////////////////////////////////////////////////////////////
};
