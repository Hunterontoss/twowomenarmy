#include "TWComponents/TW_HealthComponent.h"

UTW_HealthComponent::UTW_HealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

}

void UTW_HealthComponent::BeginPlay()
{
	Super::BeginPlay();

	SetHealth(MaxHealth);
	check(MaxHealth > 0);

	AActor* L_Owner = GetOwner();

	if (L_Owner)
	{
		L_Owner->OnTakeAnyDamage.AddDynamic(this, &UTW_HealthComponent::OnTakeAnyDamageHandle);
	}

	
}

bool UTW_HealthComponent::TryToAddHealth(const float HealthAmount)
{
	if (IsDead() || IsHealthFull())
	{
		return false;
	}

	SetHealth(Health + HealthAmount);

	return true;
}


void UTW_HealthComponent::SetHealth(const float NewHealth)
{
	const float L_NextHealth = FMath::Clamp(NewHealth, 0.f, MaxHealth);
	const float L_HealthDelta = L_NextHealth - Health;

	Health = L_NextHealth;

	OnHealthChanged.Broadcast(Health, L_HealthDelta);
}


void UTW_HealthComponent::HealUpdate()
{
	SetHealth(Health + HealModifier);

	if (IsHealthFull())
	{
		GetWorld()->GetTimerManager().ClearTimer(HealTimerHandle);
	}
}

void UTW_HealthComponent::OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	ApplyDamage(Damage, InstigatedBy);
}

bool UTW_HealthComponent::IsHealthFull() const
{
	return FMath::IsNearlyEqual(Health, MaxHealth);
}

void UTW_HealthComponent::ApplyDamage(float Damage, AController* InstigatedBy)
{
	if (Damage <= 0 || IsDead()) return;

	SetHealth(Health - Damage);
	GetWorld()->GetTimerManager().ClearTimer(HealTimerHandle);

	if (IsDead())
	{
		OnDeath.Broadcast(InstigatedBy);
	}
	else if (AutoHeal)
	{
		GetWorld()->GetTimerManager().SetTimer(HealTimerHandle, this, &UTW_HealthComponent::HealUpdate, HealUpdateTime, true, HealDelay);
	}
}





