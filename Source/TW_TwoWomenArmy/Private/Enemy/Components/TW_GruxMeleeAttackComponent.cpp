#include "Enemy/Components/TW_GruxMeleeAttackComponent.h"

#include "BehaviorTree/BlackboardComponent.h"
#include "Components/BoxComponent.h"
#include "Enemy/TW_EnemyAIController.h"
#include "Engine/SkeletalMeshSocket.h"
#include "GameFramework/Character.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"


UTW_GruxMeleeAttackComponent::UTW_GruxMeleeAttackComponent()
{
	PrimaryComponentTick.bCanEverTick = false;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Weapon 
	///
	LeftWeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Left Weapon Box"));
	RightWeaponCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("Right Weapon Box"));

	const auto* L_Owner = GetOwner<ACharacter>(); 
	if (L_Owner)
	{
		LeftWeaponCollision->SetupAttachment(L_Owner->GetMesh(), FName("LeftWeaponBone"));
		RightWeaponCollision->SetupAttachment(L_Owner->GetMesh(), FName("RightWeaponBone"));	
	}
	/////////////////////////////////////////////////////////////////////////////////////
	///
	///
}

void UTW_GruxMeleeAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	SetUpBinding();	
	SetUpCollision();

	auto* L_EnemyController = GetEnemyAIController();
	if (L_EnemyController)
	{
		L_EnemyController->GetBlackboardComponent()->SetValueAsBool(
			FName("CanAttack"),
			true);
	}
}

void UTW_GruxMeleeAttackComponent::PlayAttackMontage(FName Section, float PlayRate)
{
	const auto* L_Owner = GetOwner<ACharacter>();
	if (!L_Owner) return;
	
	UAnimInstance* AnimInstance = L_Owner->GetMesh()->GetAnimInstance();
	if (AnimInstance && AttackMontage)
	{
		AnimInstance->Montage_Play(AttackMontage);
		AnimInstance->Montage_JumpToSection(Section, AttackMontage);
	}

	bCanAttack = false;
	
	GetWorld()->GetTimerManager().SetTimer(
		AttackWaitTimer,
		this,
		&UTW_GruxMeleeAttackComponent::ResetCanAttack,
		AttackWaitTime
	);

	auto* L_EnemyController = GetEnemyAIController();
	if (L_EnemyController)
	{
		L_EnemyController->GetBlackboardComponent()->SetValueAsBool(
			FName("CanAttack"),
			false);
	}
}

FName UTW_GruxMeleeAttackComponent::GetAttackSectionName()
{
	FName SectionName;
	const int32 Section =  FMath::RandRange(1, 4);
	switch (Section)
	{
	case 1:
		SectionName = AttackLFast;
		break;
	case 2:
		SectionName = AttackRFast;
		break;
	case 3:
		SectionName = AttackL;
		break;
	case 4:
		SectionName = AttackR;
		break;
	default: break;
	}
	return SectionName;
}

void UTW_GruxMeleeAttackComponent::OnLeftWeaponOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	DoDamage(OtherActor);
	SpawnBlood(LeftWeaponSocket);
}

void UTW_GruxMeleeAttackComponent::OnRightWeaponOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	DoDamage(OtherActor);
	SpawnBlood(RightWeaponSocket);
}

void UTW_GruxMeleeAttackComponent::ActivateWeapon(const EWeaponHandType Hand)
{
	switch (Hand)
	{
	case EWeaponHandType::EAT_Left:
		LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		break;
	case EWeaponHandType::EAT_Right:
		RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		break;
	default:
		break;
	}
}

void UTW_GruxMeleeAttackComponent::DeactivateWeapon(const EWeaponHandType Hand)
{
	switch (Hand)
	{
	case EWeaponHandType::EAT_Left:
		LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	case EWeaponHandType::EAT_Right:
		RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	default:
		break;
	}
}

void UTW_GruxMeleeAttackComponent::SetUpCollision() const
{
	LeftWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	LeftWeaponCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	LeftWeaponCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	LeftWeaponCollision->SetCollisionResponseToChannel(
		ECollisionChannel::ECC_Pawn, 
		ECollisionResponse::ECR_Overlap);
	
	RightWeaponCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	RightWeaponCollision->SetCollisionObjectType(ECollisionChannel::ECC_WorldDynamic);
	RightWeaponCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
	RightWeaponCollision->SetCollisionResponseToChannel(
		ECollisionChannel::ECC_Pawn,
		ECollisionResponse::ECR_Overlap);
}

void UTW_GruxMeleeAttackComponent::SetUpBinding() const
{
	LeftWeaponCollision->OnComponentBeginOverlap.AddDynamic(
		this,
		&UTW_GruxMeleeAttackComponent::OnLeftWeaponOverlap);
	RightWeaponCollision->OnComponentBeginOverlap.AddDynamic(
		this,
		&UTW_GruxMeleeAttackComponent::OnRightWeaponOverlap);
}

void UTW_GruxMeleeAttackComponent::DoDamage(AActor* Victim)
{
	if (Victim == nullptr) return;

	
	if (Victim)
	{
		UGameplayStatics::ApplyDamage(
			Victim,
			BaseDamage,
			GetEnemyAIController(),
			GetOwner(),
			UDamageType::StaticClass()
		);

		UGameplayStatics::PlaySoundAtLocation(
				this,
				MeleeImpactSound,
				Victim->GetActorLocation());
	}
}

void UTW_GruxMeleeAttackComponent::SpawnBlood(FName SocketName) const
{
	const auto* L_Owner = GetOwner<ACharacter>();
	if (!L_Owner) return;
	
	const USkeletalMeshSocket* TipSocket = L_Owner->GetMesh()->GetSocketByName(SocketName);
	if (TipSocket)
	{
		const FTransform SocketTransform = TipSocket->GetSocketTransform(L_Owner->GetMesh());
		if (BloodParticles)
		{
			
			UGameplayStatics::SpawnEmitterAtLocation(
				GetWorld(),
				BloodParticles,
				SocketTransform
			);
		}
	}
}

void UTW_GruxMeleeAttackComponent::ResetCanAttack()
{
	bCanAttack = true;

	auto* L_EnemyController = GetEnemyAIController();
	if (L_EnemyController)
	{
		L_EnemyController->GetBlackboardComponent()->SetValueAsBool(
			FName("CanAttack"),
			true);
	}
}

ATW_EnemyAIController* UTW_GruxMeleeAttackComponent::GetEnemyAIController() const
{
	if (!GetOwner<ACharacter>()) return nullptr;

	ATW_EnemyAIController* L_EnemyAIController = GetOwner<ACharacter>()->GetController<ATW_EnemyAIController>();

	if (L_EnemyAIController)
	{
		return L_EnemyAIController;	
	}
	
	return nullptr;
}
