#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TWInterfaces/TW_BulletHitInterface.h"
#include "TW_BaseEnemy.generated.h"

class UTW_GruxMeleeAttackComponent;
class UTW_MeleeAIComponent;
class UTW_HitReactionComponent;
class UTextRenderComponent;
class UTW_EffectImpactBulletComponent;
class UTW_HealthComponent;

UCLASS()
class TW_TWOWOMENARMY_API ATW_BaseEnemy : public ACharacter, public ITW_BulletHitInterface
{
	GENERATED_BODY()

public:
	ATW_BaseEnemy();

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Components
	///
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_EffectImpactBulletComponent* ImpactComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_HealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTextRenderComponent* TextHealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_HitReactionComponent* HitReactionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_MeleeAIComponent* MeleeAIComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_GruxMeleeAttackComponent* GruxMeleeAttackComponent;

	/////////////////////////////////////////////////////////////////////////////////////
	///
	///
private:
	void SetUpCollision() const;
	/////////////////////////////////////////////////////////////////////////////////////
	/// Damage
	///
public:
	FORCEINLINE FString GetHeadBoneName() const { return HeadBone; }
protected:
	virtual void BulletHit_Implementation(FHitResult HitResult) override;

private:
	UFUNCTION()
	void OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	FString HeadBone = "head";
	/////////////////////////////////////////////////////////////////////////////////////
	/// Patrol 

	UPROPERTY(EditAnywhere, Category = "AI", meta = (AllowPrivateAccess = "true", MakeEditWidget = "true"))
	FVector PatrolPoint;

	UPROPERTY(EditAnywhere, Category = "AI", meta = (AllowPrivateAccess = "true", MakeEditWidget = "true"))
	FVector PatrolPointSecond;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Stunned
	///
public:
	UFUNCTION(BlueprintCallable)
	void SetStunned(bool Stunned);

protected:
	/** Chance of being stunned. 0: no stun chance, 1: 100% stun chance */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	float StunChance = 0.5f;
	
private:
	bool bStunned = false;


	/////////////////////////////////////////////////////////////////////////////////////
	/// Death
	///
public:
	UFUNCTION(BlueprintCallable)
	void FinishDeath();
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UAnimMontage* DeathMontage;
	
private:
	UFUNCTION()
	void OnDeathHandle(AController* InstigatedBy) const;
	/////////////////////////////////////////////////////////////////////////////////////
};
