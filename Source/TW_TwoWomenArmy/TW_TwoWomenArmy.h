#pragma once

#include "CoreMinimal.h"

#define SURFACE_METAL      EPhysicalSurface::SurfaceType1
#define SURFACE_ROCK       EPhysicalSurface::SurfaceType2
#define SURFACE_TILE       EPhysicalSurface::SurfaceType3
#define SURFACE_GRASS      EPhysicalSurface::SurfaceType4
#define SURFACE_WATER      EPhysicalSurface::SurfaceType5
#define SURFACE_OIL        EPhysicalSurface::SurfaceType6
#define SURFACE_ASPHALT    EPhysicalSurface::SurfaceType7
#define SURFACE_WOOD       EPhysicalSurface::SurfaceType8
#define SURFACE_SAND	   EPhysicalSurface::SurfaceType9