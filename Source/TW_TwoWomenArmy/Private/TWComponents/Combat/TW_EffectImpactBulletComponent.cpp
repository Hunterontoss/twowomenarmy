#include "TWComponents/Combat/TW_EffectImpactBulletComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"

UTW_EffectImpactBulletComponent::UTW_EffectImpactBulletComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTW_EffectImpactBulletComponent::BeginPlay()
{
	Super::BeginPlay();	
}

void UTW_EffectImpactBulletComponent::PlayHitEffects(const FVector Location)
{
	if (ImpactSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, Location);
		
	}
	if (ImpactParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles, Location, FRotator(0.f), true);
	}
}

void UTW_EffectImpactBulletComponent::PlayDeathEffect(const FVector Location) const
{
	if (DeathEffectParticles)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ImpactParticles, Location, FRotator(0.f), true);		
	}
	if (DeathSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, ImpactSound, Location);
		
	}
}

