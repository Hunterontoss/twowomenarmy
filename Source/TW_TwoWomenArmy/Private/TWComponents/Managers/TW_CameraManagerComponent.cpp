#include "TWComponents/Managers/TW_CameraManagerComponent.h"
#include "Camera/CameraComponent.h"
#include "Players/TW_BaseCharacter.h"

UTW_CameraManagerComponent::UTW_CameraManagerComponent() : CameraInterpDistance(250.f), CameraInterpElevation(65.f),
											bAiming(false),
											ZoomTurnRate(0.8f), ZoomLookUpRate(0.8f), CameraDefaultFOV(0.f),
											CameraCurrentFOV(0.f), CameraZoomedFOV(25.f), ZoomInterpSpeed(20.f)
{
	PrimaryComponentTick.bCanEverTick = true;
	
}

void UTW_CameraManagerComponent::BeginPlay()
{
	Super::BeginPlay();
	
	BaseCharacter = Cast<ATW_BaseCharacter>(GetOwner());
	check(BaseCharacter);
	
	if (BaseCharacter->GetCameraComponent())
	{
		CameraDefaultFOV = BaseCharacter->GetCameraComponent()->FieldOfView;
		CameraCurrentFOV = CameraDefaultFOV;	
	}	
}

void UTW_CameraManagerComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	CameraInterpZoom(DeltaTime);
}

FVector UTW_CameraManagerComponent::GetCameraInterpLocation()
{
	const FVector CameraWorldLocation = BaseCharacter->GetCameraComponent()->GetComponentLocation();
	const FVector CameraForward = BaseCharacter->GetCameraComponent()->GetForwardVector();

	return CameraWorldLocation + CameraForward * CameraInterpDistance + FVector(0.f, 0.f, CameraInterpElevation);
}

void UTW_CameraManagerComponent::CameraInterpZoom(float DeltaTime)
{
	if(bAiming)
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraZoomedFOV, DeltaTime, ZoomInterpSpeed);
	}
	else
	{
		CameraCurrentFOV = FMath::FInterpTo(CameraCurrentFOV, CameraDefaultFOV, DeltaTime, ZoomInterpSpeed);
	}
	
	BaseCharacter->GetCameraComponent()->SetFieldOfView(CameraCurrentFOV);	
}

void UTW_CameraManagerComponent::AddInterpLocations(FInterpLocation Item)
{
	InterpLocations.Add(Item);
}

FInterpLocation UTW_CameraManagerComponent::GetInterpLocation(int32 Index)
{
	if (Index <= InterpLocations.Num())
	{
		return InterpLocations[Index];
	}
	return FInterpLocation();
}

int32 UTW_CameraManagerComponent::GetInterpLocationIndex()
{
	int32 LowestIndex = 1;
	int32 LowestCount = INT_MAX;
	for (int32 i = 1; i < InterpLocations.Num(); i++)
	{
		if (InterpLocations[i].ItemCount < LowestCount)
		{
			LowestIndex = i;
			LowestCount = InterpLocations[i].ItemCount;
		}
	}

	return LowestIndex;
}

void UTW_CameraManagerComponent::IncrementInterpLocItemCount(int32 Index, int32 Amount)
{
	if (Amount < -1 || Amount > 1) return;

	if (InterpLocations.Num() >= Index)
	{
		InterpLocations[Index].ItemCount += Amount;
		
	}
}

