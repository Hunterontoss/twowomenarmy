#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "TW_BulletHitInterface.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UTW_BulletHitInterface : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class TW_TWOWOMENARMY_API ITW_BulletHitInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable)
	void BulletHit(FHitResult HitResult);
};
