// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TW_BasePlayerController.generated.h"

class UUserWidget;

UCLASS()
class TW_TWOWOMENARMY_API ATW_BasePlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ATW_BasePlayerController();

protected:
	virtual void BeginPlay() override;
	
private:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Widgets, meta=(AllowPrivateAccess="true"))
	TSubclassOf<UUserWidget> HUDOverlayClass;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Widgets, meta=(AllowPrivateAccess="true"))
	UUserWidget* HUDOverlay;	
};
