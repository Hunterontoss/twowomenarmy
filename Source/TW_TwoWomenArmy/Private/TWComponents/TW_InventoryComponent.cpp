#include "TWComponents/TW_InventoryComponent.h"
#include "TWComponents/Managers/TW_WeaponManagerComponent.h"
#include "TWItems/TW_BaseItem.h"
#include "TWItems/TW_BaseWeapon.h"

UTW_InventoryComponent::UTW_InventoryComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UTW_InventoryComponent::BeginPlay()
{
	Super::BeginPlay();
		
}

bool UTW_InventoryComponent::IsHaveFreeSpace() const
{
	return WeaponInventory.Num() < Inventory_Capacity;
}

bool UTW_InventoryComponent::IsInventoryFull() const
{
	return WeaponInventory.Num() >= Inventory_Capacity;
}

void UTW_InventoryComponent::AddWeaponToInventory(ATW_BaseWeapon* WeaponToAdd)
{
	if (IsHaveFreeSpace())
	{
		WeaponToAdd->SlotIndex = WeaponInventory.Num();
		WeaponInventory.Add(WeaponToAdd);
		WeaponToAdd->SetItemState(EItemState::EIS_PickedUp);
	}
}

void UTW_InventoryComponent::SwapWeaponSlotIndex(ATW_BaseItem* EquippedWeapon, ATW_BaseItem* WeaponToSwap)
{
	if (WeaponInventory.Num() - 1 >= EquippedWeapon->SlotIndex)
	{
		WeaponInventory[EquippedWeapon->SlotIndex] = WeaponToSwap;
		WeaponToSwap->SlotIndex = EquippedWeapon->SlotIndex;
	}
}

void UTW_InventoryComponent::ExchangeInventoryWeapon(int32 NewItemIndex)
{
	auto WeaponManager = GetOwner()->FindComponentByClass<UTW_WeaponManagerComponent>();

	if (WeaponManager)
	{
		const int32 CurrentItemIndex = WeaponManager->CurrentEquippedWeapon->SlotIndex;
		auto* EquippedWeapon = WeaponManager->CurrentEquippedWeapon;

		if (CurrentItemIndex == NewItemIndex || NewItemIndex >= WeaponInventory.Num() || (EquippedWeapon->GetCombatState() != ECombatState::ECS_Unoccupied)) return;
		auto OldEquippedWeapon = EquippedWeapon;
		auto NewWeapon = Cast<ATW_BaseWeapon>(WeaponInventory[NewItemIndex]);
		WeaponManager->EquipWeapon(NewWeapon);

		OldEquippedWeapon->SetItemState(EItemState::EIS_PickedUp);
		NewWeapon->SetItemState(EItemState::EIS_Equipped);

		NewWeapon->StartEquip();
		NewWeapon->PlayEquipSound();
	}
}

int32 UTW_InventoryComponent::GetEmptyInventorySlot()
{
	for (int i = 0; i < WeaponInventory.Num(); ++i)
	{
		if (WeaponInventory[i] == nullptr)
		{
			return i;
		}
	}

	if (WeaponInventory.Num() < Inventory_Capacity)
	{
		return WeaponInventory.Num();
	}

	return -1;
}
