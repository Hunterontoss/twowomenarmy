// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "TWItems/TW_ItemTypes.h"
#include "TW_BaseItem.generated.h"

class UTW_ItemEffectsComponent;
class USoundCue;
class ATW_BaseCharacter;
class UWidgetComponent;
class UBoxComponent;
class USphereComponent;
class UCurveFloat;

UCLASS()
class TW_TWOWOMENARMY_API ATW_BaseItem : public AActor
{
	GENERATED_BODY()
	
public:	
	ATW_BaseItem();
	virtual void Tick(float DeltaTime) override;

	FORCEINLINE UWidgetComponent* GetPickupWidget() const { return  PickupWidget;}
	FORCEINLINE USphereComponent* GetAreaSphere() const { return AreaSphere;}
	FORCEINLINE UBoxComponent* GetBoxCollision() const { return BoxCollision;}
	FORCEINLINE USkeletalMeshComponent* GetItemMesh() const { return ItemMesh; }
	FORCEINLINE EItemState GetItemState() const { return ItemState; }
	FORCEINLINE USoundCue* GetPickupSound() const { return PickupSound; }
	FORCEINLINE USoundCue* GetEquipSound() const { return EquipSound; }
	FORCEINLINE int32 GetItemCount() const { return ItemCount; }
	FORCEINLINE FTimerHandle GetItemInterpTimer() const { return ItemInterpTimer;}
	
	void SetItemState(EItemState State);
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory)
	int32 SlotIndex;

protected:
	virtual void BeginPlay() override;
	virtual void OnConstruction(const FTransform& Transform) override;
	virtual void SetItemProperties(EItemState State);
	
	UFUNCTION()
	void OnSphereOverlap(UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
	
	UFUNCTION()
	void OnSphereEndOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex);

	/////////////////////////////////////////////////////////////////////////////////////
	/// Components
	///
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UTW_ItemEffectsComponent* ItemEffectsComp;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	USkeletalMeshComponent* ItemMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Components)
	UBoxComponent* BoxCollision;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= Components)
	UWidgetComponent* PickupWidget;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category= Components)
	USphereComponent* AreaSphere;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Item Params
	///
protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Item Properties")
	FString ItemName;

	/** Item count (ammo, etc.) */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Properties")
	int32 ItemCount;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Rarity
	///
protected:
	void SetActiveStars();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Rarity)
	EItemRarity ItemRarity;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties")
	EItemState ItemState;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties")
	TArray<bool> ActiveStars;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Item Pickup Interpolation
	///
public:
	void StartItemCurve(ATW_BaseCharacter* Player);

protected:
	void FinishInterping();
	void ItemInterp(float DeltaTime);
	FVector GetInterpLocation();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	USoundCue* PickupSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	USoundCue* EquipSound;
	
private:
	/** The curve asset to use for the item's Z location when interping */
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category="Item Properties", meta=(AllowPrivateAccess="true"))
	UCurveFloat* ItemZCurve;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	UCurveFloat* ItemScaleCurve;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	FVector ItemInterpStartLocation;
	
	/** Target interp location in front of the camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	FVector CameraTargetLocation;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	bool bInterping;
	
	FTimerHandle ItemInterpTimer;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	float ZCurveTime;

	/** X and Y for the Item while interping in the EquipInterping state */
	float ItemInterpX;
	float ItemInterpY;
	float InterpInitialYawOffset;

	/** Index of the interp location this item is interping to */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	int32 InterpLocIndex;
	
	/////////////////////////////////////////////////////////////////////////////////////

public:
	void PlayPickupSound() const;
	void PlayEquipSound() const;
	
protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	EItemType ItemType;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties", meta = (AllowPrivateAccess = "true"))
	ATW_BaseCharacter* Character;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Material 
	///

public:
	virtual void EnableCustomDepth();
	virtual void DisableCustomDepth();
	
	void EnableGlowMaterial();
	void DisableGlowMaterial();
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Item Properties")
	UMaterialInstanceDynamic* DynamicMaterialInstance;
	
protected:
	virtual void InitializeCustomDepth();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Properties")
	int32 MaterialIndex;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item Properties")
	UMaterialInstance* MaterialInstance;

private:
	bool bCanChangeCustomDepth;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Item Icons 
	///

protected:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Rarity)
	FLinearColor GlowColor;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Rarity)
	FLinearColor LightColor;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Rarity)
	FLinearColor DarkColor;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Rarity)
	int32 NumberOfStars;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Rarity)
	UTexture2D* IconBackground;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Inventory)
	UTexture2D* IconItem;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Inventory)
	UTexture2D* IconAmmo;
	/////////////////////////////////////////////////////////////////////////////////////
	/// Data Table
	///

private:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = DataTable, meta=(AllowPrivateAccess = "true"))
	UDataTable* ItemRarityDataTable;
};
