#include "Enemy/TW_ExplosionBarrel.h"
#include "TWComponents/TW_HealthComponent.h"
#include "TWComponents/Combat/TW_EffectImpactBulletComponent.h"

ATW_ExplosionBarrel::ATW_ExplosionBarrel()
{
	PrimaryActorTick.bCanEverTick = true;

	ImpactComponent = CreateDefaultSubobject<UTW_EffectImpactBulletComponent>(TEXT("ImpactComp"));
	HealthComponent = CreateDefaultSubobject<UTW_HealthComponent>(TEXT("HealthComp"));
}
void ATW_ExplosionBarrel::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent)
	
	HealthComponent->OnDeath.AddUObject(this, &ATW_ExplosionBarrel::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &ATW_ExplosionBarrel::OnHealthChanged);
}

void ATW_ExplosionBarrel::BulletHit_Implementation(FHitResult HitResult)
{
	ImpactComponent->PlayHitEffects(HitResult.Location);
}

void ATW_ExplosionBarrel::OnDeath(AController* InstigatedBy)
{
	ImpactComponent->PlayDeathEffect(GetActorLocation());
	Destroy();
}

void ATW_ExplosionBarrel::OnHealthChanged(float Health, float HealthDelta)
{
}
