#include "Enemy/AI/TW_MeleeAIComponent.h"
#include "DrawDebugHelpers.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/SphereComponent.h"
#include "Enemy/TW_EnemyAIController.h"
#include "GameFramework/Character.h"
#include "Kismet/KismetMathLibrary.h"
#include "Players/TW_BaseCharacter.h"

DEFINE_LOG_CATEGORY_STATIC(LogMeleeAIComponent, All, All)

UTW_MeleeAIComponent::UTW_MeleeAIComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
	
	AgroSphere = CreateDefaultSubobject<USphereComponent>(TEXT("AgroSphere"));
	CombatRangeSphere = CreateDefaultSubobject<USphereComponent>(TEXT("CombatRangeSphere"));
	
	if (GetOwner<ACharacter>())
	{
		AgroSphere->SetupAttachment(GetOwner<ACharacter>()->GetRootComponent());
		CombatRangeSphere->SetupAttachment(GetOwner<ACharacter>()->GetRootComponent());
	}

	AgroSphere->SetSphereRadius(512.f);
	CombatRangeSphere->SetSphereRadius(128.f);
}

void UTW_MeleeAIComponent::BeginPlay()
{
	Super::BeginPlay();

	check(GetOwner())
	
	if (GetOwner<ACharacter>())
	{
		AgroSphere->OnComponentBeginOverlap.AddDynamic(
		this, 
		&UTW_MeleeAIComponent::AgroSphereOverlap);

		CombatRangeSphere->OnComponentBeginOverlap.AddDynamic(
			this,
			&UTW_MeleeAIComponent::CombatRangeOverlap);
		
		CombatRangeSphere->OnComponentEndOverlap.AddDynamic(
			this,
			&UTW_MeleeAIComponent::CombatRangeEndOverlap);
	}
}

void UTW_MeleeAIComponent::MakePatrol(const FVector PatrolPoint, const FVector SecondPatrolPoint)
{
	const FTransform OwnerTransform =  GetOwner<ACharacter>()->GetActorTransform();

	
	const FVector WorldPatrolPoint = UKismetMathLibrary::TransformLocation(
		OwnerTransform, 
		PatrolPoint
		);

	const FVector WorldPatrolPointSecond = UKismetMathLibrary::TransformLocation(
		OwnerTransform,
		SecondPatrolPoint
		);
	
	DrawDebugSphere(
		GetWorld(),
		WorldPatrolPoint,
		25.f,
		12,
		FColor::Red,
		true
	);

	DrawDebugSphere(
		GetWorld(),
		WorldPatrolPointSecond,
		25.f,
		12,
		FColor::Red,
		true
	);

	auto* L_EnemyAIController = GetEnemyAIController();
	if (L_EnemyAIController)
	{
		
		L_EnemyAIController->GetBlackboardComponent()->SetValueAsVector(
			TEXT("PatrolPoint"), WorldPatrolPoint);

		L_EnemyAIController->GetBlackboardComponent()->SetValueAsVector(
			TEXT("PatrolPoint2"), WorldPatrolPointSecond);
		
		L_EnemyAIController->RunBehaviorTree(BehaviorTree);
	}
}

void UTW_MeleeAIComponent::MoveTo(AActor* Target)
{
	auto* L_EnemyAIController = GetEnemyAIController();
	if (L_EnemyAIController && Target)
	{
		L_EnemyAIController->GetBlackboardComponent()->SetValueAsObject(
			TEXT("Target"), Target);
	}
}

void UTW_MeleeAIComponent::Death()
{
	if (GetEnemyAIController())
	{
		GetEnemyAIController()->GetBlackboardComponent()->SetValueAsBool(
			TEXT("Dead"), 
			true);
	}
}

void UTW_MeleeAIComponent::AgroSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
                                             UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == nullptr) return;
	
	UObject* Character = Cast<ATW_BaseCharacter>(OtherActor);
	if (Character && GetEnemyAIController())
	{
		GetEnemyAIController()->GetBlackboardComponent()->SetValueAsObject(
			TEXT("Target"), 
			Character);
	}
}

void UTW_MeleeAIComponent::MakeStunned(bool Stunned) const
{
	if (GetEnemyAIController())
	{
		GetEnemyAIController()->GetBlackboardComponent()->SetValueAsBool(
			TEXT("Stunned"), 
			Stunned);
	}
}

void UTW_MeleeAIComponent::CombatRangeOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == nullptr) return;
	const UObject* Character = Cast<ATW_BaseCharacter>(OtherActor);;
	if (Character)
	{
		bInAttackRange = true;
		if (GetEnemyAIController())
		{
			GetEnemyAIController()->GetBlackboardComponent()->SetValueAsBool(
				TEXT("InAttackRange"),
				true
			);
		}
	}
}

void UTW_MeleeAIComponent::CombatRangeEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == nullptr) return;
	const UObject* Character = Cast<ATW_BaseCharacter>(OtherActor);;
	if (Character)
	{
		bInAttackRange = false;
		if (GetEnemyAIController())
		{
			GetEnemyAIController()->GetBlackboardComponent()->SetValueAsBool(
				TEXT("InAttackRange"),
				false
			);
		}
	}
}

ATW_EnemyAIController* UTW_MeleeAIComponent::GetEnemyAIController() const
{
	if (!GetOwner<ACharacter>()) return nullptr;

	ATW_EnemyAIController* EnemyAIController = GetOwner<ACharacter>()->GetController<ATW_EnemyAIController>();

	if (EnemyAIController)
	{
		return EnemyAIController;	
	}
	
	return nullptr;
}

