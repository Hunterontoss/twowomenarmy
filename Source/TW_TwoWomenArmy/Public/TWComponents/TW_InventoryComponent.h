#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_InventoryComponent.generated.h"

class ATW_BaseItem;
class ATW_BaseWeapon;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_InventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_InventoryComponent();

protected:
	virtual void BeginPlay() override;

public:

	UFUNCTION(BlueprintCallable)
	bool IsInventoryFull() const;
	
	bool IsHaveFreeSpace() const;
	void AddWeaponToInventory(ATW_BaseWeapon* WeaponToAdd);
	void SwapWeaponSlotIndex(ATW_BaseItem* EquippedWeapon, ATW_BaseItem* WeaponToSwap);
	void ExchangeInventoryWeapon(int32 NewItemIndex);

	int32 GetEmptyInventorySlot();
	
private:

	const int32 Inventory_Capacity = 4;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Inventory, meta=(AllowPrivateAccess="true"))
	TArray<ATW_BaseItem*> WeaponInventory;
};
