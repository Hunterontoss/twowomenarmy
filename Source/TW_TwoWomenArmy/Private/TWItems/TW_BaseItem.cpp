#include "TWItems/TW_BaseItem.h"
#include "Camera/CameraComponent.h"
#include "Components/BoxComponent.h"
#include "Components/SphereComponent.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Players/TW_BaseCharacter.h"
#include "Sound/SoundCue.h"
#include "TWComponents/Managers/TW_CameraManagerComponent.h"
#include "TWComponents/Managers/TW_ShooterCharacterManager.h"
#include "TWItems/TW_ItemEffectsComponent.h"

ATW_BaseItem::ATW_BaseItem() : SlotIndex(0), ItemName(FString("Default")), ItemCount(0),
    ItemRarity(EItemRarity::EIR_Common), ItemState(EItemState::EIS_Pickup), ItemInterpStartLocation(FVector(0.f)), CameraTargetLocation(FVector(0.f)),
    bInterping(false), ZCurveTime(0.7f), ItemInterpX(0.f), ItemInterpY(0.f),
    InterpInitialYawOffset(0.f),InterpLocIndex(0),ItemType(EItemType::EIT_MAX), MaterialIndex(0),
    bCanChangeCustomDepth(true)
{
	PrimaryActorTick.bCanEverTick = true;

	ItemMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("ItemMesh"));
	SetRootComponent(ItemMesh);

	BoxCollision = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionComponent"));
	BoxCollision->SetupAttachment(GetRootComponent());
	BoxCollision->SetCollisionResponseToChannels(ECR_Ignore);
	BoxCollision->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	PickupWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("PickupWidget"));
	PickupWidget->SetupAttachment(GetRootComponent());
	PickupWidget->SetDrawSize(FVector2D(600, 200));
	
	AreaSphere = CreateDefaultSubobject<USphereComponent>(TEXT("AreaSphere"));
	AreaSphere->SetupAttachment(GetRootComponent());
	AreaSphere->SetSphereRadius(128.f);

	ItemEffectsComp = CreateDefaultSubobject<UTW_ItemEffectsComponent>(TEXT("ItemEffectsComp"));
}

void ATW_BaseItem::BeginPlay()
{
	Super::BeginPlay();

	if (PickupWidget)
	{
		PickupWidget->SetVisibility(false);
	}

	AreaSphere->OnComponentBeginOverlap.AddDynamic(this, &ATW_BaseItem::OnSphereOverlap);
	AreaSphere->OnComponentEndOverlap.AddDynamic(this, &ATW_BaseItem::OnSphereEndOverlap);

	SetActiveStars();
	SetItemProperties(ItemState);
	InitializeCustomDepth();

	ItemEffectsComp->StartPulseTimer();
}

void ATW_BaseItem::OnConstruction(const FTransform& Transform)
{
	Super::OnConstruction(Transform);
	
	const FString RarityTablePath(TEXT("DataTable'/Game/_TwoWomenArmy/DataTables/ItemRarityDataTable.ItemRarityDatatable'"));
	const UDataTable* RarityTableObject = Cast<UDataTable>(StaticLoadObject(UDataTable::StaticClass(), nullptr, *RarityTablePath));

	if (RarityTableObject)
	{
		FItemRarityTable* RarityRow = nullptr;
		switch (ItemRarity)
		{
		case EItemRarity::EIR_Damaged:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName("Damaged"), TEXT(""));
			break;
		case EItemRarity::EIR_Common:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName("Common"), TEXT(""));
			break;
		case EItemRarity::EIR_Uncommon:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName("Uncommon"), TEXT(""));
			break;
		case EItemRarity::EIR_Rare:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName("Rare"), TEXT(""));
			break;
		case EItemRarity::EIR_Legendary:
			RarityRow = RarityTableObject->FindRow<FItemRarityTable>(FName("Legendary"), TEXT(""));
			break;
		default:
			break;
		}

		if (RarityRow)
		{
			GlowColor = RarityRow->GlowColor;
			LightColor = RarityRow->LightColor;
			DarkColor = RarityRow->DarkColor;
			NumberOfStars = RarityRow->NumberOfStars;
			IconBackground = RarityRow->IconBackground;

			if (GetItemMesh())
			{
				GetItemMesh()->SetCustomDepthStencilValue(RarityRow->CustomDepthStencil);
			}
		}
	}

	if (MaterialInstance)
	{
		DynamicMaterialInstance = UMaterialInstanceDynamic::Create(MaterialInstance, this);
		DynamicMaterialInstance->SetVectorParameterValue(TEXT("FresnelColor"), GlowColor);
		ItemMesh->SetMaterial(MaterialIndex, DynamicMaterialInstance);

		EnableGlowMaterial();
	}
}

void ATW_BaseItem::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ItemInterp(DeltaTime);
	// Get curve values from PulseCurve and set dynamic material parameters
	ItemEffectsComp->UpdatePulse();
}


void ATW_BaseItem::SetItemState(EItemState State)
{
	ItemState = State;
	SetItemProperties(State);
}

void ATW_BaseItem::SetItemProperties(EItemState State)
{
	switch (State)
	{
	case EItemState::EIS_Pickup:
		// Set mesh properties
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set AreaSphere properties
		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Overlap);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
		// Set CollisionBox properties
		BoxCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		BoxCollision->SetCollisionResponseToChannel(
			ECollisionChannel::ECC_Visibility,
			ECollisionResponse::ECR_Block);
		BoxCollision->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		break;
	case EItemState::EIS_Equipped:
		// Set mesh properties
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set AreaSphere properties
		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set CollisionBox properties
		BoxCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		BoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	case EItemState::EIS_Falling:
		// Set mesh properties
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
		ItemMesh->SetSimulatePhysics(true);
		ItemMesh->SetEnableGravity(true);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionResponseToChannel(
			ECollisionChannel::ECC_WorldStatic,
			ECollisionResponse::ECR_Block);
		// Set AreaSphere properties
		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set CollisionBox properties
		BoxCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		BoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	case EItemState::EIS_EquipInterping:
		PickupWidget->SetVisibility(false);
		// Set mesh properties
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(true);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set AreaSphere properties
		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set CollisionBox properties
		BoxCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		BoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	case EItemState::EIS_PickedUp:
		PickupWidget->SetVisibility(false);
		// Set mesh properties
		ItemMesh->SetSimulatePhysics(false);
		ItemMesh->SetEnableGravity(false);
		ItemMesh->SetVisibility(false);
		ItemMesh->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		ItemMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set AreaSphere properties
		AreaSphere->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		AreaSphere->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		// Set CollisionBox properties
		BoxCollision->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);
		BoxCollision->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		break;
	default:
		break;
	}
}

void ATW_BaseItem::OnSphereOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor)
	{
		ATW_BaseCharacter* BaseCharacter = Cast<ATW_BaseCharacter>(OtherActor);

		if (BaseCharacter)
		{
			BaseCharacter->AddOverlappedItemCount(1);
		}
	}
}

void ATW_BaseItem::OnSphereEndOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor,
	UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor)
	{
		ATW_BaseCharacter* BaseCharacter = Cast<ATW_BaseCharacter>(OtherActor);

		if (BaseCharacter)
		{
			BaseCharacter->AddOverlappedItemCount(-1);
			BaseCharacter->UnHighlightInventorySlot();
		}
	}
}

void ATW_BaseItem::SetActiveStars()
{
	// The 0 element isn't used
	for (int32 i = 0; i<=5; i++)
	{
		ActiveStars.Add(false);
	}

	switch (ItemRarity)
	{
	case EItemRarity::EIR_Damaged:
		ActiveStars[1] = true;
		break;
	case EItemRarity::EIR_Common:
		ActiveStars[1] = true;
		ActiveStars[2] = true;
		break;
	case EItemRarity::EIR_Uncommon:
		ActiveStars[1] = true;
		ActiveStars[2] = true;
		ActiveStars[3] = true;
		break;
	case EItemRarity::EIR_Rare:
		ActiveStars[1] = true;
		ActiveStars[2] = true;
		ActiveStars[3] = true;
		ActiveStars[4] = true;
		break;
	case EItemRarity::EIR_Legendary:
		ActiveStars[1] = true;
		ActiveStars[2] = true;
		ActiveStars[3] = true;
		ActiveStars[4] = true;
		ActiveStars[5] = true;
		break;
	default:
		ActiveStars[1] = true;	
	}	
}

void ATW_BaseItem::StartItemCurve(ATW_BaseCharacter* Player)
{
	Character = Player;

	// Get array index in InterpLocations with the lowest item count
	InterpLocIndex = Character->TWCameraManager->GetInterpLocationIndex();
	// Add 1 to the Item Count for this interp location struct
	Character->TWCameraManager->IncrementInterpLocItemCount(InterpLocIndex, 1);

	PlayPickupSound();
	
	ItemInterpStartLocation = GetActorLocation();
	bInterping = true;
	SetItemState(EItemState::EIS_EquipInterping);
	GetWorldTimerManager().ClearTimer(ItemEffectsComp->PulseTimer);

	GetWorldTimerManager().SetTimer(ItemInterpTimer, this, &ATW_BaseItem::FinishInterping, ZCurveTime);

	const float CameraRotationYaw = Character->GetCameraComponent()->GetComponentRotation().Yaw;
	const float ItemRotationYaw = GetActorRotation().Yaw;
	
	InterpInitialYawOffset = ItemRotationYaw - CameraRotationYaw;
	bCanChangeCustomDepth = false;
}

void ATW_BaseItem::FinishInterping()
{
	bInterping = false;
	if (Character)
	{
		Character->TWCameraManager->IncrementInterpLocItemCount(InterpLocIndex, -1);
		Character->GetPickupItem(this);
		PlayEquipSound();
		Character->UnHighlightInventorySlot();
	}
	SetActorScale3D(FVector::OneVector);

	DisableGlowMaterial();
	bCanChangeCustomDepth = true;
	DisableCustomDepth();
}

void ATW_BaseItem::ItemInterp(float DeltaTime)
{
	if (!bInterping) return;

	if (Character && ItemZCurve)
	{
		const float ElapsedTime = GetWorldTimerManager().GetTimerElapsed(ItemInterpTimer);
		const float CurveValue = ItemZCurve->GetFloatValue(ElapsedTime);

		FVector ItemLocation = ItemInterpStartLocation;
		
		//const FVector CameraInterpLocation = Character->TWCameraManager->GetCameraInterpLocation();
		const FVector CameraInterpLocation  = GetInterpLocation();
		
		const FVector ItemToCamera = FVector(0.f, 0.f, (CameraInterpLocation - ItemLocation).Z);
		const float DeltaZ = ItemToCamera.Size();

		const FVector CurrentLocation = GetActorLocation();

		const float InterpXValue =
			FMath::FInterpTo(CurrentLocation.X, CameraInterpLocation.X, DeltaTime, 30.f);

		const float InterpYValue =
			FMath::FInterpTo(CurrentLocation.Y, CameraInterpLocation.Y, DeltaTime, 30.f);

		ItemLocation.X = InterpXValue;
		ItemLocation.Y = InterpYValue;
		ItemLocation.Z += CurveValue  * DeltaZ;
		
		SetActorLocation(ItemLocation, true, nullptr, ETeleportType::TeleportPhysics);

		const FRotator CameraRotation = Character->GetCameraComponent()->GetComponentRotation();
		const FRotator ItemRotation = FRotator(0.f, CameraRotation.Yaw + InterpInitialYawOffset, 0.f);
		
		SetActorRotation(ItemRotation, ETeleportType::TeleportPhysics);

		if (ItemScaleCurve)
		{
			const float ScaleCurveValue = ItemScaleCurve->GetFloatValue(ElapsedTime);
			SetActorScale3D(FVector(ScaleCurveValue, ScaleCurveValue, ScaleCurveValue));
		}
	}
}

FVector ATW_BaseItem::GetInterpLocation()
{
	if (Character == nullptr) return FVector::ZeroVector;

	switch (ItemType)
	{
	case EItemType::EIT_Ammo:
		return Character->TWCameraManager->GetInterpLocation(InterpLocIndex).SceneComponent->GetComponentLocation();
	case EItemType::EIT_Weapon:
		return Character->TWCameraManager->GetInterpLocation(0).SceneComponent->GetComponentLocation();
	default:
		return FVector();
	}
}

void ATW_BaseItem::PlayPickupSound() const
{
	if (Character)
	{
		if (Character->TWCharacterManager->ShouldPlayPickupSound())
		{
			Character->TWCharacterManager->StartPickupSoundTimer();
			if (PickupSound)
			{
				UGameplayStatics::PlaySound2D(this, PickupSound);
			}
		}
	}
}

void ATW_BaseItem::PlayEquipSound() const
{
	const ATW_BaseCharacter* BaseCharacter = Cast<ATW_BaseCharacter>(GetOwner());
	if (BaseCharacter)
	{
		if (BaseCharacter->TWCharacterManager->ShouldPlayEquipSound())
		{
			BaseCharacter->TWCharacterManager->StartEquipSoundTimer();
			if (EquipSound)
			{
				UGameplayStatics::PlaySound2D(this, EquipSound);
			}
		}
	}
}

void ATW_BaseItem::EnableCustomDepth()
{
	if (bCanChangeCustomDepth)
	{
		ItemMesh->SetRenderCustomDepth(true);	
	}
}

void ATW_BaseItem::DisableCustomDepth()
{
	if (bCanChangeCustomDepth)
	{
		ItemMesh->SetRenderCustomDepth(false);	
	}
}

void ATW_BaseItem::InitializeCustomDepth()
{
	DisableCustomDepth();
}

void ATW_BaseItem::EnableGlowMaterial()
{
	if (DynamicMaterialInstance)
	{
		DynamicMaterialInstance->SetScalarParameterValue(TEXT("GlowBlendAlpha"), 0);
	}
}

void ATW_BaseItem::DisableGlowMaterial()
{
	if (DynamicMaterialInstance)
	{
		DynamicMaterialInstance->SetScalarParameterValue(TEXT("GlowBlendAlpha"), 1);
	}
		
}
