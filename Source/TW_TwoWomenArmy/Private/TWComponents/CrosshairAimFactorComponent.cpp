#include "TWComponents/CrosshairAimFactorComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Players/TW_BaseCharacter.h"


UCrosshairAimFactorComponent::UCrosshairAimFactorComponent() : CrosshairSpreadMultiplier(0.f),
													CrosshairVelocityFactor(0.f), CrosshairAimFactor(0.f),
													CrosshairShootingFactor(0.f)
{
	
}

void UCrosshairAimFactorComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UCrosshairAimFactorComponent::CalculateCrosshairSpread(float DeltaTime)
{
	CrosshairVelocityFactor = GetVelocityFactor();
	CrosshairInAirFactor = GetInAirFactor(DeltaTime);
	CrosshairAimFactor = GetAimFactor(DeltaTime);

	CrosshairSpreadMultiplier = 0.5f + CrosshairVelocityFactor + CrosshairInAirFactor - CrosshairAimFactor;
}


float UCrosshairAimFactorComponent::GetCrosshairSpreadMultiplier() const
{
	return CrosshairSpreadMultiplier;
}

float UCrosshairAimFactorComponent::GetVelocityFactor() const
{
	const FVector2D WalkSpeedRange(0.f, 600.f);
	const FVector2D VelocityMultiplierRange(0.f, 1.f);
	
	return FMath::GetMappedRangeValueClamped(WalkSpeedRange, VelocityMultiplierRange, Velocity.Size());
}

float UCrosshairAimFactorComponent::GetInAirFactor(float DeltaTime) const
{
	float SpreadFactor;
	if (bIsFalling)
	{
		SpreadFactor = FMath::FInterpTo(CrosshairInAirFactor, 2.25f, DeltaTime, 2.25f);
	}
	else
	{
		SpreadFactor = FMath::FInterpTo(CrosshairInAirFactor, 0.f, DeltaTime,30.f);
	}
	
	return SpreadFactor;
}

float UCrosshairAimFactorComponent::GetAimFactor(float DeltaTime) const
{
	float SpreadFactor = 0;
	if (bIsAiming)
	{
		SpreadFactor = FMath::FInterpTo(CrosshairAimFactor, 0.6f, DeltaTime, 30.f);
	}
	
	return SpreadFactor;
}

float UCrosshairAimFactorComponent::GetShootingFactor()
{
	return 0.f;
}

