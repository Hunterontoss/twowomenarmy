// Fill out your copyright notice in the Description page of Project Settings.


#include "Enemy/TW_GruxAnimInstance.h"
#include "Enemy/TW_BaseEnemy.h"

void UTW_GruxAnimInstance::UpdateAnimationProperties(float DeltaTime)
{
	if (Enemy == nullptr)
	{
		Enemy = Cast<ATW_BaseEnemy>(TryGetPawnOwner());
	}

	if (Enemy)
	{
		FVector Velocity =  Enemy->GetVelocity();
		Velocity.Z = 0.f;
		Speed = Velocity.Size();
	}

}
