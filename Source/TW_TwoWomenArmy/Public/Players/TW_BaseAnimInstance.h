// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimInstance.h"
#include "TWItems/TW_WeaponTypes.h"
#include "TW_BaseAnimInstance.generated.h"

class ATW_BaseCharacter;

UENUM(BlueprintType)
enum class EOffsetState : uint8
{
	EOS_Aiming UMETA(DisplayName = "Aiming"),
	EOS_Hip UMETA(DisplayName = "Hip"),
	EOS_Reloading UMETA(DisplayName = "Reloading"),
	EOS_InAir UMETA(DisplayName = "InAir"),


	EOS_MAX UMETA(DisplayName = "DefaultMAX")
};

UCLASS()
class TW_TWOWOMENARMY_API UTW_BaseAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UTW_BaseAnimInstance();
	
	virtual void NativeInitializeAnimation() override;

	UFUNCTION(BlueprintCallable)
	void UpdateAnimationProperties(float DeltaTime);

	///////////////////////////////////////////////////////////////////////////////
	/// Turn In Place Character
	///
	
protected:
	void TurnInPlace();

private:

	void SetTurningRecoilParam();
	float CharacterYaw;
	float CharacterYawLastFrame;
	
	float RotationCurveThisFrame;
	float RotationCurveLastFrame;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turn In Place", meta = (AllowPrivateAccess = "true"))
	float RootYawOffset;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turn In Place", meta = (AllowPrivateAccess = "true"))
	float Pitch;
	
	/** True when reloading, used to prevent Aim Offset while reloading */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turn In Place", meta = (AllowPrivateAccess = "true"))
	bool bReloading;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
    bool bTurningInPlace;
	
	///////////////////////////////////////////////////////////////////////////////
	/// Variable for Animation
	///

private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess="true"))
	ATW_BaseCharacter* BaseCharacter;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float Speed;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bIsInAir;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bIsAccelerating;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Crouching, meta = (AllowPrivateAccess = "true"))
	bool bCrouching;

    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
    float RecoilWeight;
	///////////////////////////////////////////////////////////////////////////////
	/// Strafing
	///

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float MovementOffsetYaw;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	float LastMovementOffsetYaw;

	///////////////////////////////////////////////////////////////////////////////
	/// Aim
	///
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	bool bAiming;

	///////////////////////////////////////////////////////////////////////////////
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Turn In Place", meta = (AllowPrivateAccess = "true"))
	EOffsetState OffsetState;


	///////////////////////////////////////////////////////////////////////////////
	/// Lean
	///
protected:
	void Lean(float DeltaTime);
	
private:
	
	/** Character Yaw this frame */
	FRotator CharacterRotation;

	/** Character Yaw last frame */
	FRotator CharacterRotationLastFrame;

	/** Yaw delta used for leaning in the running blendspace */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Lean, meta = (AllowPrivateAccess = "true"))
	float LeanYawDelta;

	///////////////////////////////////////////////////////////////////////////////
	/// Equipping
	///

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, meta = (AllowPrivateAccess = "true"))
	bool bEquipping;
	
	///////////////////////////////////////////////////////////////////////////////
	/// Weapon 
	///

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	EWeaponType EquippedWeaponType;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	bool bShouldUseFABRIK;

	///////////////////////////////////////////////////////////////////////////////
	void SetBoolFlagsForAnimations();

	///////////////////////////////////////////////////////////////////////////////
	/// Rotate Hips
	///
	
protected:	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Hips)
	bool bShouldTurnHips;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Hips)
	bool RunningBackwards;
	
private:
	void UpdateTurnHipsFlag();
	///////////////////////////////////////////////////////////////////////////////
	
};
