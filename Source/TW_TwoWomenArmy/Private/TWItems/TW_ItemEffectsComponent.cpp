#include "TWItems/TW_ItemEffectsComponent.h"
#include "Curves/CurveVector.h"
#include "TWItems/TW_BaseItem.h"

UTW_ItemEffectsComponent::UTW_ItemEffectsComponent() : PulseCurveTime(5.f), GlowAmount(150.f),
                                                       FresnelExponent(3.f),FresnelReflectFraction(4.f)
	
{
	PrimaryComponentTick.bCanEverTick = false;
	
}

void UTW_ItemEffectsComponent::BeginPlay()
{
	Super::BeginPlay();
}

void UTW_ItemEffectsComponent::OnRegister()
{
	Super::OnRegister();

	ItemOwner = Cast<ATW_BaseItem>(GetOwner());
}

void UTW_ItemEffectsComponent::StartPulseTimer()
{
	if (ItemOwner->GetItemState() == EItemState::EIS_Pickup)
	{
		GetWorld()->GetTimerManager().SetTimer(PulseTimer, this, &UTW_ItemEffectsComponent::ResetPulseTimer, PulseCurveTime);
	}
}

void UTW_ItemEffectsComponent::ResetPulseTimer()
{
	StartPulseTimer();
}

void UTW_ItemEffectsComponent::UpdatePulse()
{
	if (!ItemOwner) return;
	
	float ElapsedTime;
	FVector CurveValue = FVector();
	
	switch (ItemOwner->GetItemState())
	{
	case EItemState::EIS_Pickup:
		if (PulseCurve)
		{
			ElapsedTime = GetWorld()->GetTimerManager().GetTimerElapsed(PulseTimer);
			CurveValue = PulseCurve->GetVectorValue(ElapsedTime);
		}
		break;
	case EItemState::EIS_EquipInterping:
		if (InterpPulseCurve)
		{
			ElapsedTime = GetWorld()->GetTimerManager().GetTimerElapsed(ItemOwner->GetItemInterpTimer());
			CurveValue = InterpPulseCurve->GetVectorValue(ElapsedTime);
		}
		break;
	default:
		break;
	}
	
	if (ItemOwner->DynamicMaterialInstance)
	{
		ItemOwner->DynamicMaterialInstance->SetScalarParameterValue(TEXT("GlowAmount"), CurveValue.X * GlowAmount);
		ItemOwner->DynamicMaterialInstance->SetScalarParameterValue(TEXT("FresnelExponent"), CurveValue.Y * FresnelExponent);
		ItemOwner->DynamicMaterialInstance->SetScalarParameterValue(TEXT("FresnelReflectFraction"), CurveValue.Z * FresnelReflectFraction);
	}
}
