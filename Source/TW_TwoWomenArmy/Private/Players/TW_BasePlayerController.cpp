// Fill out your copyright notice in the Description page of Project Settings.


#include "Players/TW_BasePlayerController.h"
#include "Blueprint/UserWidget.h"

ATW_BasePlayerController::ATW_BasePlayerController()
{
	
}

void ATW_BasePlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (HUDOverlayClass)
	{
		HUDOverlay = CreateWidget<UUserWidget>(this, HUDOverlayClass);

		if (HUDOverlay)
		{
			HUDOverlay->AddToViewport();
			HUDOverlay->SetVisibility(ESlateVisibility::Visible);
		}
	}
}
