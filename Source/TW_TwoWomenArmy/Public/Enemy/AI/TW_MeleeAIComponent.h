// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_MeleeAIComponent.generated.h"


class ATW_EnemyAIController;
class UBehaviorTree;
class USphereComponent;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_MeleeAIComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_MeleeAIComponent();

protected:
	virtual void BeginPlay() override;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Getters / Setters  
	///
	
public:
	FORCEINLINE UBehaviorTree* GetBehaviorTree() const { return BehaviorTree; }
	ATW_EnemyAIController* GetEnemyAIController() const;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Behavior Tree
	///
	
private:
	UPROPERTY(EditAnywhere, Category = "Behavior Tree", meta = (AllowPrivateAccess = "true"))
	UBehaviorTree* BehaviorTree;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Commands
	///

public:
	virtual void MakePatrol(const FVector PatrolPoint, const FVector SecondPatrolPoint);
	virtual void MoveTo(AActor* Target);
	virtual void Death();
	/////////////////////////////////////////////////////////////////////////////////////
	/// AGRO
	///
protected:
	UFUNCTION()
	void AgroSphereOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
	
private:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat, meta = (AllowPrivateAccess = "true"))
	USphereComponent* AgroSphere;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Stun
	///

public:
	void MakeStunned(bool Stunned) const;
	

	/////////////////////////////////////////////////////////////////////////////////////
	/// Melee Attack
	///
public:
	UFUNCTION()
	void CombatRangeOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex,
		bool bFromSweep,
		const FHitResult& SweepResult);
	UFUNCTION()
	void CombatRangeEndOverlap(
		UPrimitiveComponent* OverlappedComponent,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex);

protected:
	/** True when in attack range; time to attack! */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Combat)
	bool bInAttackRange;

	/** Sphere for attack range */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	USphereComponent* CombatRangeSphere;

	/////////////////////////////////////////////////////////////////////////////////////
};

