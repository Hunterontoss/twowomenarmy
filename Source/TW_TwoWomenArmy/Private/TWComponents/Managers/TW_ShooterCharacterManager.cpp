#include "TWComponents/Managers/TW_ShooterCharacterManager.h"
#include "Players/TW_BaseCharacter.h"

UTW_ShooterCharacterManager::UTW_ShooterCharacterManager() : bShouldPlayPickupSound(true),
	bShouldPlayEquipSound(true), PickupSoundResetTime(0.2f), EquipSoundResetTime(0.2f)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTW_ShooterCharacterManager::BeginPlay()
{
	Super::BeginPlay();
	
	OwnerCharacter = Cast<ATW_BaseCharacter>(GetOwner());

	check(OwnerCharacter)
	
}

void UTW_ShooterCharacterManager::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

void UTW_ShooterCharacterManager::StartPickupSoundTimer()
{
	bShouldPlayPickupSound = false;
	GetWorld()->GetTimerManager().SetTimer(
		PickupSoundTimer, 
		this, 
		&UTW_ShooterCharacterManager::ResetPickupSoundTimer, 
		PickupSoundResetTime);
}

void UTW_ShooterCharacterManager::StartEquipSoundTimer()
{
	bShouldPlayEquipSound = false;
	GetWorld()->GetTimerManager().SetTimer(
		EquipSoundTimer,
		this,
		&UTW_ShooterCharacterManager::ResetEquipSoundTimer,
		EquipSoundResetTime);
}

void UTW_ShooterCharacterManager::ResetPickupSoundTimer()
{
	bShouldPlayPickupSound = true;
}

void UTW_ShooterCharacterManager::ResetEquipSoundTimer()
{
	bShouldPlayEquipSound = true;
}


