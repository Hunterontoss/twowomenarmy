// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CrosshairAimFactorComponent.generated.h"

class ATW_BaseCharacter;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UCrosshairAimFactorComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UCrosshairAimFactorComponent();
	
	UFUNCTION(BlueprintCallable)
	float GetCrosshairSpreadMultiplier() const;
	
	float GetVelocityFactor() const;
	float GetInAirFactor(float DeltaTime) const;
	float GetAimFactor(float DeltaTime) const;
	float GetShootingFactor();

	void CalculateCrosshairSpread(float DeltaTime);

	FVector Velocity;
	bool bIsFalling;
	bool bIsAiming;
	
	
protected:
	virtual void BeginPlay() override;
	
private:
	/////////////////////////////////////////////////////////////////////////////////////
	/// Факторы которые влияют на расширение прицела.
	///
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Crosshair, meta = (AllowPrivateAccess="true"))
	float CrosshairSpreadMultiplier;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Crosshair, meta = (AllowPrivateAccess="true"))
	float CrosshairVelocityFactor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Crosshair, meta = (AllowPrivateAccess="true"))
	float CrosshairInAirFactor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Crosshair, meta = (AllowPrivateAccess = "true"))
	float CrosshairAimFactor;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Crosshair, meta = (AllowPrivateAccess = "true"))
	float CrosshairShootingFactor;
		
};
