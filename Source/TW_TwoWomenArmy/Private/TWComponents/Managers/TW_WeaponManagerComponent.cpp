#include "TWComponents/Managers/TW_WeaponManagerComponent.h"
#include "Engine/SkeletalMeshSocket.h"
#include "Players/TW_BaseCharacter.h"
#include "TWComponents/TW_InventoryComponent.h"
#include "TWItems/TW_BaseAmmo.h"
#include "TWItems/TW_BaseWeapon.h"

UTW_WeaponManagerComponent::UTW_WeaponManagerComponent() : Basic9mmAmmoCount(90), Basic556mmAmmoCount(60),
	HighlightedSlot(-1)
{
	PrimaryComponentTick.bCanEverTick = true;
}

void UTW_WeaponManagerComponent::BeginPlay()
{
	Super::BeginPlay();

	InitializeAmmoMap();
	EquipWeapon(SpawnDefaultWeapon());

	if (CurrentEquippedWeapon)
	{
		OwnerCharacter->TWInventoryComp->AddWeaponToInventory(CurrentEquippedWeapon);
		CurrentEquippedWeapon->SlotIndex = 0;
		CurrentEquippedWeapon->DisableCustomDepth();
		CurrentEquippedWeapon->DisableGlowMaterial();
		CurrentEquippedWeapon->SetItemState(EItemState::EIS_Equipped);
	}
}

void UTW_WeaponManagerComponent::OnRegister()
{
	Super::OnRegister();

	OwnerCharacter = Cast<ATW_BaseCharacter>(GetOwner());
	check(OwnerCharacter);

}

void UTW_WeaponManagerComponent::StartFire()
{
	if (!CurrentEquippedWeapon) return;
	
	const auto WorldTime =  GetWorld()->TimeSeconds;
	const auto FirstDelay = FMath::Max(LastTimeFire + CurrentEquippedWeapon->GetTimeBetweenShot() - WorldTime, 0.f);

	
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_TimeBetweenShots, this,
										  &UTW_WeaponManagerComponent::CurrentWeaponFire,
									CurrentEquippedWeapon->GetTimeBetweenShot(), true, FirstDelay);
}

void UTW_WeaponManagerComponent::StopFire()
{
	GetWorld()->GetTimerManager().ClearTimer(TimerHandle_TimeBetweenShots);
}

bool UTW_WeaponManagerComponent::IsUnoccupied() const
{
	if (CurrentEquippedWeapon)
	{
		return CurrentEquippedWeapon->GetCombatState() == ECombatState::ECS_Unoccupied;
	}
	return false;
}

bool UTW_WeaponManagerComponent::IsFireTimerInProgress() const
{
	if (CurrentEquippedWeapon)
	{
		return CurrentEquippedWeapon->GetCombatState() == ECombatState::ECS_FireTimerInProgress;
	}
	return false;
}

void UTW_WeaponManagerComponent::CurrentWeaponFire()
{
	if (!CurrentEquippedWeapon) return;

	if (WeaponHasAmmo())
	{
		CurrentEquippedWeapon->Fire();
		LastTimeFire = GetWorld()->TimeSeconds;

		// TODO:: Make Better;
		if (!CurrentEquippedWeapon->IsAutomatic())
		{
			StopFire();
		}
	}
	else if (CarryingAmmo())
	{
		CurrentEquippedWeapon->StartReload();
	}
}

void UTW_WeaponManagerComponent::EquipWeapon(ATW_BaseWeapon* WeaponToEquip, const bool bSwapping)
{
	if (WeaponToEquip)
	{
		if(OwnerCharacter)
		{
			const USkeletalMeshSocket* WeaponSocket = OwnerCharacter->GetMesh()->
													  GetSocketByName(FName("WeaponSocket"));
			if (WeaponSocket)
			{
				WeaponSocket->AttachActor(WeaponToEquip, OwnerCharacter->GetMesh());
			}

			if (CurrentEquippedWeapon == nullptr)
			{
				// -1 == no EquippedWeapon yet. No need to reverse the icon animation
				EquipItemDelegate.Broadcast(-1, WeaponToEquip->SlotIndex);
			}
			else if (!bSwapping)
			{
				EquipItemDelegate.Broadcast(CurrentEquippedWeapon->SlotIndex, WeaponToEquip->SlotIndex);
			}

			CurrentEquippedWeapon = WeaponToEquip;
			CurrentEquippedWeapon->SetItemState(EItemState::EIS_Equipped);
			CurrentEquippedWeapon->SetOwner(OwnerCharacter);

			if (OwnerCharacter->GetAiming())
			{
				OwnerCharacter->StopAim();
			}
		}
		
	}
}

void UTW_WeaponManagerComponent::DropWeapon()
{
	if (CurrentEquippedWeapon)
	{
		const FDetachmentTransformRules DetachmentTransformRules(EDetachmentRule::KeepWorld, true);
		CurrentEquippedWeapon->GetItemMesh()->DetachFromComponent(DetachmentTransformRules);

		CurrentEquippedWeapon->SetItemState(EItemState::EIS_Falling);
		CurrentEquippedWeapon->ThrowWeapon();
		StopFire();
		CurrentEquippedWeapon->SetOwner(nullptr);
		CurrentEquippedWeapon = nullptr;
	}
}

void UTW_WeaponManagerComponent::SwapWeapon(ATW_BaseWeapon* WeaponToSwap)
{
	if (!OwnerCharacter) return;
	
	OwnerCharacter->TWInventoryComp->SwapWeaponSlotIndex(CurrentEquippedWeapon, WeaponToSwap);

	DropWeapon();
	EquipWeapon(WeaponToSwap, true);
}

ATW_BaseWeapon* UTW_WeaponManagerComponent::SpawnDefaultWeapon() const
{	
	if (DefaultWeaponClass)
	{
		return GetWorld()->SpawnActor<ATW_BaseWeapon>(DefaultWeaponClass);
	}
	
	return nullptr;
}

void UTW_WeaponManagerComponent::InitializeAmmoMap()
{
	AmmoMap.Add(EAmmoType::EAT_9mm, Basic9mmAmmoCount);
	AmmoMap.Add(EAmmoType::EAT_556mm, Basic556mmAmmoCount);
}

void UTW_WeaponManagerComponent::PickupAmmo(ATW_BaseAmmo* Ammo)
{
	if (AmmoMap.Find(Ammo->GetAmmoType()))
	{
		int32 AmmoCount = AmmoMap[Ammo->GetAmmoType()];
		AmmoCount += Ammo->GetItemCount();
		AmmoMap[Ammo->GetAmmoType()] = AmmoCount;
	}

	if (CurrentEquippedWeapon->GetAmmoType() == Ammo->GetAmmoType())
	{
		if (CurrentEquippedWeapon->GetAmmo() == 0)
		{
			CurrentEquippedWeapon->StartReload();
		}
	}

	Ammo->Destroy();
}

bool UTW_WeaponManagerComponent::WeaponHasAmmo()
{
	if (!CurrentEquippedWeapon) return false;

	return CurrentEquippedWeapon->GetAmmo() > 0;
}

void UTW_WeaponManagerComponent::DecrementAmmo()
{
	if (CurrentEquippedWeapon)
	{
		CurrentEquippedWeapon->DecrementAmmo();
	}
}

void UTW_WeaponManagerComponent::ReloadCurrentAmmo()
{
	if (CurrentEquippedWeapon)
	{
		if (CarryingAmmo() && CurrentEquippedWeapon->ClipIsFull())
		{
			CurrentEquippedWeapon->StartReload();	
		}
	}
}

bool UTW_WeaponManagerComponent::IsReloading() const
{
	if (CurrentEquippedWeapon)
	{
		return CurrentEquippedWeapon->GetCombatState() == ECombatState::ECS_Reloading;
	}
	return false;
}

bool UTW_WeaponManagerComponent::IsEquipping() const
{
	if (CurrentEquippedWeapon)
	{
		return CurrentEquippedWeapon->GetCombatState() == ECombatState::ECS_Equipping;
	}
	return false;
}

void UTW_WeaponManagerComponent::GrabClip()
{
	if (!CurrentEquippedWeapon && !OwnerCharacter && !OwnerCharacter->HandSceneComponent) return;

	const auto* WeaponMesh = CurrentEquippedWeapon->GetItemMesh();
	const auto ClipBoneName =  CurrentEquippedWeapon->GetClipBoneName();
	
	const int32 ClipBoneIndex = WeaponMesh->GetBoneIndex(ClipBoneName);
	ClipTransform = WeaponMesh->GetBoneTransform(ClipBoneIndex);

	const FAttachmentTransformRules AttachmentRules(EAttachmentRule::KeepRelative, true);
	OwnerCharacter->HandSceneComponent->AttachToComponent(OwnerCharacter->GetMesh(), AttachmentRules, FName(TEXT("Hand_L")));
	OwnerCharacter->HandSceneComponent->SetWorldTransform(ClipTransform);

	CurrentEquippedWeapon->SetMovingClip(true);
}

void UTW_WeaponManagerComponent::ReleaseClip()
{
	if (CurrentEquippedWeapon)
	{
		CurrentEquippedWeapon->SetMovingClip(false);
	}
}

void UTW_WeaponManagerComponent::FinishReloading()
{
	CurrentEquippedWeapon->SetCombatState(ECombatState::ECS_Unoccupied);
	if (OwnerCharacter->GetAimingButtonPressed())
	{
		OwnerCharacter->StartAim();
	}
	if (!CurrentEquippedWeapon) return;
	const auto AmmoType = CurrentEquippedWeapon->GetAmmoType();
	
	if (AmmoMap.Contains(AmmoType))
	{
		int32 EquippedAmmo = AmmoMap[AmmoType];
		
		const int32 MagazineEmptySpace = CurrentEquippedWeapon->GetMagazineCapacity() - CurrentEquippedWeapon->GetAmmo();

		if (MagazineEmptySpace > EquippedAmmo)
		{
			// Reload the Magazine with all the ammo we are carrying
			CurrentEquippedWeapon->ReloadAmmo(EquippedAmmo);
			EquippedAmmo = 0;
			AmmoMap.Add(AmmoType, EquippedAmmo);
		}
		else
		{
			CurrentEquippedWeapon->ReloadAmmo(MagazineEmptySpace);
			EquippedAmmo -= MagazineEmptySpace;
			AmmoMap.Add(AmmoType, EquippedAmmo);
		}
	}
}

void UTW_WeaponManagerComponent::FinishEquipping()
{
	if (CurrentEquippedWeapon)
	{
		CurrentEquippedWeapon->SetCombatState(ECombatState::ECS_Unoccupied);

		if (OwnerCharacter)
		{
			if (OwnerCharacter->GetAimingButtonPressed())
			{
				OwnerCharacter->StartAim();
			}	
		}
	}
}

int UTW_WeaponManagerComponent::GetAmmoCount()
{
	if (CurrentEquippedWeapon)
	{
		return CurrentEquippedWeapon->GetAmmo();
	}
	return 0;
}

bool UTW_WeaponManagerComponent::CarryingAmmo()
{
	if (!CurrentEquippedWeapon) return false;

	const auto AmmoType = CurrentEquippedWeapon->GetAmmoType();

	if (AmmoMap.Contains(AmmoType))
	{
		return AmmoMap[AmmoType] > 0;
	}
	return false;
}


