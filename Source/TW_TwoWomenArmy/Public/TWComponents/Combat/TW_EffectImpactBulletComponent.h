#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_EffectImpactBulletComponent.generated.h"

class UParticleSystem;
class USoundCue;

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_EffectImpactBulletComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_EffectImpactBulletComponent();

	UFUNCTION(BlueprintCallable)
	void PlayHitEffects(const FVector Location);
	void PlayDeathEffect(const FVector Location) const;
protected:
	virtual void BeginPlay() override;

	/////////////////////////////////////////////////////////////////////////////////////
	///
	///

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UParticleSystem* ImpactParticles;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	USoundCue* ImpactSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	USoundCue* DeathSound;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Combat)
	UParticleSystem* DeathEffectParticles;
	/////////////////////////////////////////////////////////////////////////////////////
};
