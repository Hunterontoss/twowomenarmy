#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TW_HealthComponent.generated.h"

DECLARE_MULTICAST_DELEGATE_OneParam(FOnDeathSignature, AController*);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHealthChanged, float, float);


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class TW_TWOWOMENARMY_API UTW_HealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UTW_HealthComponent();

protected:
	virtual void BeginPlay() override;


	/////////////////////////////////////////////////////////////////////////////////////
	/// Stats
	///
public:

	FOnDeathSignature OnDeath;
	FOnHealthChanged OnHealthChanged;
	
	FORCEINLINE float GetHealth() const { return Health;}

	UFUNCTION(BlueprintCallable, Category = Health)
	float GetHealthWithPercent() const { return Health / MaxHealth; }
	
	UFUNCTION(BlueprintCallable, Category = Health)
	bool IsDead() const { return FMath::IsNearlyZero(Health); }

	bool TryToAddHealth(float HealthAmount);	
protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category=Stats)
	float MaxHealth = 100.f;
	
private:
	void SetHealth(float NewHealth);
	
	float Health = 0.f;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Auto Heal
	///
public:
	bool IsHealthFull() const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal")
	bool AutoHeal = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
	float HealUpdateTime = 1.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
	float HealDelay = 2.4f;
	
	// How many Health for autoheal;
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta = (EditCondition = "AutoHeal"))
	float HealModifier = 5.0f;

private:

	void HealUpdate();
	FTimerHandle HealTimerHandle;
	
	/////////////////////////////////////////////////////////////////////////////////////
	/// Damage 
	///
private:
	UFUNCTION()
	void OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser);

	void ApplyDamage(float Damage, AController* InstigatedBy);

	/////////////////////////////////////////////////////////////////////////////////////
	
};
