#pragma once

#include "CoreMinimal.h"
#include "TWItems/TW_BaseItem.h"
#include "TW_CoreTypes.h"
#include "TW_BaseAmmo.generated.h"

class USphereComponent;

UCLASS()
class TW_TWOWOMENARMY_API ATW_BaseAmmo : public ATW_BaseItem
{
	GENERATED_BODY()

public:
	ATW_BaseAmmo();
	virtual void Tick(float DeltaSeconds) override;

	FORCEINLINE UStaticMeshComponent* GetAmmoMesh() const { return AmmoMesh; }
	FORCEINLINE EAmmoType GetAmmoType() const { return AmmoType; }
	
protected:
	virtual void BeginPlay() override;

	virtual void SetItemProperties(EItemState State) override;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Ammo)
	USphereComponent* AmmoCollisionSphere;

	UFUNCTION()
	void AmmoSphereOverlap(UPrimitiveComponent* OverlappedComponent,
						   AActor* OtherActor,
						   UPrimitiveComponent* OtherComp,
						   int32 OtherBodyIndex,
		                   bool bFromSweep,
		                   const FHitResult& SweepResult);
	
private:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Ammo, meta = (AllowPrivateAccess = "true"))
	UStaticMeshComponent* AmmoMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo, meta = (AllowPrivateAccess = "true"))
	EAmmoType AmmoType;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Ammo, meta = (AllowPrivateAccess = "true"))
	UTexture2D* AmmoIconTexture;

	/////////////////////////////////////////////////////////////////////////////////////
	/// Material 
	///

	virtual void EnableCustomDepth() override;
	virtual void DisableCustomDepth() override;
};
