#include "Enemy/TW_BaseEnemy.h"

#include "DrawDebugHelpers.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/TextRenderComponent.h"
#include "Enemy/TW_EnemyAIController.h"
#include "Enemy/AI/TW_MeleeAIComponent.h"
#include "Enemy/Components/TW_GruxMeleeAttackComponent.h"
#include "TWComponents/TW_HealthComponent.h"
#include "TWComponents/Combat/TW_EffectImpactBulletComponent.h"
#include "TWComponents/Combat/TW_HitReactionComponent.h"

ATW_BaseEnemy::ATW_BaseEnemy()
{
	PrimaryActorTick.bCanEverTick = true;

	ImpactComponent = CreateDefaultSubobject<UTW_EffectImpactBulletComponent>(TEXT("ImpactEffectComponent"));
	HealthComponent = CreateDefaultSubobject<UTW_HealthComponent>(TEXT("HealthComp"));
	HitReactionComponent = CreateDefaultSubobject<UTW_HitReactionComponent>(TEXT("HitReactionComp"));
	
	TextHealthComponent = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextHealthComp"));
	TextHealthComponent->SetupAttachment(GetRootComponent());

	MeleeAIComponent = CreateDefaultSubobject<UTW_MeleeAIComponent>(TEXT("MeleeAIComp"));
	GruxMeleeAttackComponent = CreateDefaultSubobject<UTW_GruxMeleeAttackComponent>(TEXT("GruxAttack"));
}

void ATW_BaseEnemy::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent)
	check(MeleeAIComponent)
	check(GruxMeleeAttackComponent)
	
	SetUpCollision();
	
	MeleeAIComponent->MakePatrol(PatrolPoint, PatrolPointSecond);

	OnTakeAnyDamage.AddDynamic(this, &ATW_BaseEnemy::OnTakeAnyDamageHandle);
	HealthComponent->OnDeath.AddUObject(this, &ATW_BaseEnemy::OnDeathHandle);
}

void ATW_BaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	const auto Health = HealthComponent->GetHealth();
	TextHealthComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));
}

void ATW_BaseEnemy::SetUpCollision() const
{
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Visibility, ECollisionResponse::ECR_Block);
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECollisionResponse::ECR_Ignore);
	GetCapsuleComponent()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Camera, ECR_Ignore);
}

void ATW_BaseEnemy::BulletHit_Implementation(FHitResult HitResult)
{
	if (HealthComponent->IsDead()) return;
	
	ImpactComponent->PlayHitEffects(HitResult.Location);

	// TODO::AnimationWithDirection	
	const float Stunned = FMath::FRandRange(0.f, 1.f);
	if (Stunned <= StunChance)
	{
		HitReactionComponent->PlayHitMontage(FName("HitReact_B"));
		SetStunned(true);
	}
}

void ATW_BaseEnemy::SetStunned(bool Stunned)
{
	bStunned = Stunned;

	MeleeAIComponent->MakeStunned(bStunned);
}

void ATW_BaseEnemy::OnTakeAnyDamageHandle(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
	AController* InstigatedBy, AActor* DamageCauser)
{
	MeleeAIComponent->MoveTo(InstigatedBy);
}


void ATW_BaseEnemy::FinishDeath()
{
	GetMesh()->bPauseAnims = true;
	
	SetLifeSpan(3.f);
}

void ATW_BaseEnemy::OnDeathHandle(AController* InstigatedBy) const
{
	GetCapsuleComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	MeleeAIComponent->Death();
	
	UAnimInstance* AnimInstance = GetMesh()->GetAnimInstance();
	if (AnimInstance && DeathMontage)
	{
		AnimInstance->Montage_Play(DeathMontage);
	}
}

