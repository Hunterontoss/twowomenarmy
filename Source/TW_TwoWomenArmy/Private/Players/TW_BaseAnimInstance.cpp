#include "Players/TW_BaseAnimInstance.h"
#include "Players/TW_BaseCharacter.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "TWComponents/Managers/TW_WeaponManagerComponent.h"

UTW_BaseAnimInstance::UTW_BaseAnimInstance() : CharacterYaw(0.f),	CharacterYawLastFrame(0.f), RootYawOffset(0.f),
   Pitch(0.f),	bReloading(false), bTurningInPlace(false), Speed(0.f), bIsInAir(false), bIsAccelerating(false),
   RecoilWeight(1.f), MovementOffsetYaw(0.f), LastMovementOffsetYaw(0.f), bAiming(false),
   OffsetState(EOffsetState::EOS_Hip), CharacterRotation(FRotator::ZeroRotator), CharacterRotationLastFrame(FRotator::ZeroRotator),
   LeanYawDelta(0.f), EquippedWeaponType(EWeaponType::EWT_MAX), bShouldUseFABRIK(false)
{
	
}

void UTW_BaseAnimInstance::NativeInitializeAnimation()
{
	Super::NativeInitializeAnimation();

	BaseCharacter = Cast<ATW_BaseCharacter>(TryGetPawnOwner());

}

void UTW_BaseAnimInstance::UpdateAnimationProperties(float DeltaTime)
{
	if (!BaseCharacter)
	{
		BaseCharacter = Cast<ATW_BaseCharacter>(TryGetPawnOwner());
	}

	if (BaseCharacter)
	{
		SetBoolFlagsForAnimations();
		
		FVector Velocity = BaseCharacter->GetVelocity();
		Velocity.Z = 0;
		Speed = Velocity.Size();

		bIsInAir = BaseCharacter->GetMovementComponent()->IsFalling();

		if (BaseCharacter->GetCharacterMovement()->GetCurrentAcceleration().Size() > 0.f)
		{
			bIsAccelerating = true;
		}
		else
		{
			bIsAccelerating = false;
		}

		const FRotator AimRotation = BaseCharacter->GetBaseAimRotation();
		const FRotator MovementRotation = UKismetMathLibrary::MakeRotFromX(BaseCharacter->GetVelocity());

		MovementOffsetYaw = UKismetMathLibrary::NormalizedDeltaRotator(MovementRotation, AimRotation).Yaw;

		if (BaseCharacter->GetVelocity().Size() > 0.f)
		{
			LastMovementOffsetYaw = MovementOffsetYaw;
		}

		bAiming = BaseCharacter->GetAiming();
		
		if (bReloading)
		{
			OffsetState = EOffsetState::EOS_Reloading;
		}
		else if (bIsInAir)
		{
			OffsetState = EOffsetState::EOS_InAir;
		}
		else if (BaseCharacter->GetAiming())
		{
			OffsetState = EOffsetState::EOS_Aiming;
		}
		else
		{
			OffsetState = EOffsetState::EOS_Hip;
		}
		
		if (BaseCharacter->WeaponManager && BaseCharacter->WeaponManager->CurrentEquippedWeapon)
		{
			EquippedWeaponType = BaseCharacter->WeaponManager->CurrentEquippedWeapon->GetWeaponType();
		}
	}
	
	TurnInPlace();
	Lean(DeltaTime);

	UpdateTurnHipsFlag();
}

void UTW_BaseAnimInstance::TurnInPlace()
{
	if (!BaseCharacter) return;

	Pitch = BaseCharacter->GetBaseAimRotation().Pitch;
	
	if (Speed > 0 || bIsInAir)
	{
		RootYawOffset = 0.f;
		CharacterYaw = BaseCharacter->GetActorRotation().Yaw;
		CharacterYawLastFrame = CharacterYaw;
		RotationCurveLastFrame = 0.f;
		RotationCurveThisFrame = 0.f;
	}
	else
	{
		CharacterYawLastFrame = CharacterYaw;
		CharacterYaw = BaseCharacter->GetActorRotation().Yaw;
		const float YawDelta = CharacterYaw - CharacterYawLastFrame;

		RootYawOffset = UKismetMathLibrary::NormalizeAxis(RootYawOffset - YawDelta);

		// 1.0 if turning, 0.0 if not
		const float Turning{ GetCurveValue(TEXT("Turning")) };
		if (Turning > 0)
		{
			if (GEngine) GEngine->AddOnScreenDebugMessage(1, -1, FColor::Cyan, FString::Printf(TEXT("RootYawOffset: %f"), RootYawOffset));
            bTurningInPlace = true;
			RotationCurveLastFrame = RotationCurveThisFrame;
			RotationCurveThisFrame = GetCurveValue(TEXT("DistanceCurve"));
			//RotationCurveThisFrame = GetCurveValue(TEXT("Rotation"));
			const float DeltaRotation{ RotationCurveThisFrame - RotationCurveLastFrame };

			// RootYawOffset > 0, -> Turning Left. RootYawOffset < 0, -> Turning Right.
			RootYawOffset > 0 ? RootYawOffset -= DeltaRotation : RootYawOffset += DeltaRotation;

			const float ABSRootYawOffset{ FMath::Abs(RootYawOffset) };
			if (ABSRootYawOffset > 90.f)
			{
				const float YawExcess{ ABSRootYawOffset - 90.f };
				RootYawOffset > 0 ? RootYawOffset -= YawExcess : RootYawOffset += YawExcess;
			}
		}
        else
        {
            bTurningInPlace = false;
        }
	}
	SetTurningRecoilParam();
}

void UTW_BaseAnimInstance::SetTurningRecoilParam()
{
	if (bTurningInPlace)
	{
		bReloading || bEquipping ? RecoilWeight = 1.f : RecoilWeight = 0.f;
	}
	else
	{
		if (bCrouching)
		{
			bReloading  || bEquipping ? RecoilWeight = 1.f : RecoilWeight = 0.1f;
		}
		else
		{
			bAiming || bReloading || bEquipping ? RecoilWeight = 1.f : RecoilWeight = 0.5f;
		}
	}
}

void UTW_BaseAnimInstance::Lean(float DeltaTime)
{
	
	if (!BaseCharacter) return;
	CharacterRotationLastFrame = CharacterRotation;
	CharacterRotation = BaseCharacter->GetActorRotation();

	const FRotator Delta{ UKismetMathLibrary::NormalizedDeltaRotator(CharacterRotation, CharacterRotationLastFrame) };

	const float Target{ Delta.Yaw / DeltaTime };
	const float Interp{ FMath::FInterpTo(LeanYawDelta, Target, DeltaTime, 6.f) };
	LeanYawDelta = FMath::Clamp(Interp, -90.f, 90.f);
}

void UTW_BaseAnimInstance::SetBoolFlagsForAnimations()
{
	if (!BaseCharacter) return;
	
	bCrouching = BaseCharacter->GetCrouching();

	const auto* WeaponManager = BaseCharacter->WeaponManager;
	if (WeaponManager)
	{
		bReloading = WeaponManager->IsReloading();
		bEquipping = WeaponManager->IsEquipping();
		bShouldUseFABRIK = WeaponManager->IsFireTimerInProgress() || WeaponManager->IsUnoccupied();	
	}
}

void UTW_BaseAnimInstance::UpdateTurnHipsFlag()
{
	// !!! call after update MovementOffsetYaw flag !!!
	if (FMath::Abs(MovementOffsetYaw) < 60.f && !bCrouching && bIsAccelerating)
	{
		bShouldTurnHips = true;
	}
	else
	{
		bShouldTurnHips = false;
	}

	if (FMath::Abs(MovementOffsetYaw) > 120.f && !bCrouching && bIsAccelerating)
	{
		RunningBackwards = true;
	}
	else
	{
		RunningBackwards = false;
	}
}
